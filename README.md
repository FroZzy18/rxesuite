# RXESuite #

RXESuite is an application developed by me which acts as a big part of my Master thesis at the Vienna University of Economics and Business (WU Wien). The main focus of this application is to provide a mean for creating a common infrastructure for imperative and declarative process mining on relational databases. It leverages different technologies and findings in the field of process mining to derive data structures (declarative constraints, imperative ordering relations) used for process analysis. Please refer to my Master thesis which will be available at the university for more information about this application.

### How do I get set up? ###

The repository contains all the necessary source code and library files for compiling the application. The application has to be compiled with the Java SE Development Kit 8 for a respective platform.

Additionally, I also provide a pre-compiled, ready-to-run version of the application. It is part of an overall demonstration-scenario of RXESuite that I designed to show the capabilities of RXESuite in a fast way. All files needed for the demonstration of the application are stored in the "demonstration" directory of this repository.

### Demonstration ###

Requirements: 

- Java SE Development Kit 8 or the Java SE Runtime Environment 8 needs to be installed
- Administrator access to a Microsoft SQL Server (probably also works with less rights on the server, but it was only tested with administrator privileges)

This demonstration will guide you through the steps required to set up a common infrastructure for process mining and to export derived data structures for imperative as well as declarative process mining as CSV files. 

1. Download the repository (or just the demonstration folder) to your local harddrive.
2. Double click on RXESuite.jar to start the application.
3. You should now see the main window of RXESuite. Please click on the Database Configuration tab to set up the required information for connecting to a database.
![Demonstration_Database_Configuration.PNG](https://bitbucket.org/repo/M8xKye/images/3173969002-Demonstration_Database_Configuration.PNG)

4. Fill out the fields with the necessary information for connecting to the Microsoft SQL database. You can also save the entered information by specifying a "Config-Name" in the according textbox and clicking on the "Save Config" button.
5. Click on the Import tab to switch to the import feature of RXESuite.
![Demonstration_Import.PNG](https://bitbucket.org/repo/M8xKye/images/3553313296-Demonstration_Import.PNG)

6. Here it is possible to set up a database with the schema required for the application and populate it with information from an XES event log file. For this demonstration I have included the 2 event logs also used in my Master thesis in the example_logs folder. Please click on the "File..." button and select the "example.xes" file from the aforementioned folder.
7. Make sure that the name of the database is specified (should be done automatically) and to select the "Create new Database" radio button, since a completely new database should be set up as well as populated.
8. Click on the "Start Import" button to start the setting up / import process. 
![Demonstration_Import_MessagesPNG.PNG](https://bitbucket.org/repo/M8xKye/images/2130074695-Demonstration_Import_MessagesPNG.PNG)

9. After the process has finished (signaled by the progress bar at the bottom reaching 100% and a status message appearing stating that the import was successful) it is possible to derive the declarative constraints and the imperative ordering relations. Click on the Main tab of the application.
10. Make sure that the database name is filled in (should be done automatically) and that the "name of activity field" is set to the name of the activities in the respective event log. For the example log the default content: "concept:name" is fine. You can then also specify thresholds for displaying declarative constraints, but I recommend leaving that setting to 0. Click on the "Derive Constraints and Relations" button to start with the derivation process.
11. After a couple of seconds you should see the final result: the table in the center of the application got populated with declarative constraints. Additionally, it is possible to select a specific imperative process mining algorithm from the combobox at the left-hand-side of the application to show ordering relations for the respective algorithm. 
![Demonstration_Derive_Constraints.PNG](https://bitbucket.org/repo/M8xKye/images/2793858338-Demonstration_Derive_Constraints.PNG)

12. As a last step, those derived constraints and ordering relations can be exported as a sturctured CSV file. This can be done by clicking on the buttons on the right hand side of the application and selecting a target directory. In the selected directory a CSV file (seperation character is a semicolon ;) is created containing the respective elements from the table in the center.

The exported CSV files can then be used for further analysis. For example, they could be important in any spreadsheet application to execute custom analyses. However, it is also possible to used those files directly as an input for specialized process mining applications that take the exported CSV files as an input to create a process model. To proof this point by example, I developed another application that takes the CSV file of exported Alpha algorithm ordering relations as an input and uses the Alpha algorithm to create a graphical process model. This application is called RXESAlphaMiner and can be accessed on: https://bitbucket.org/FroZzy18/rxesalphaminer/ .

Last but not least, the "demonstration" directory also contains a folder called "screenshots" which contains screenshots of the tool that were taken during a demonstration procedure. They illustrate the necessary steps for setting up a common infrastructure for process mining and exporting the derived constraints and relations as CSV files. 

### SQL ###

My application uses SQL queries for deriving declarative constraints together with their Support and Confidence values (i.e., metrics for process mining). In their original form, those queries are part of a scientific publication (https://arxiv.org/abs/1512.00196). I use adapted versions of those queries to derive the declarative constraints which are afterwards used to derive imperative ordering relations. The exact queries that I used are stored as .sql files in the "sql" folder of this repository.

### Diagrams ###

The repository also contains a directory called "diagrams" which contains a summarized UML 2.0 Class Diagram of the application and an ER diagram of the database structure after creating a new database with RXESuite.