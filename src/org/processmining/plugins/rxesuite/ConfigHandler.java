package org.processmining.plugins.rxesuite;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class ConfigHandler{
	
	final static String[] supportedDBTypes = {"MySQL/MariaDB", "PostgreSQL", "Oracle", "MicrosoftSQL", "SQLite"};
	
	private List<DatabaseConfig> configs;
	private File configFile;
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	public ConfigHandler(File configFile) throws IOException{
		if (configFile.exists() && !configFile.isDirectory()) {
			
			BufferedReader reader = new BufferedReader(new FileReader(configFile));
			Type collectionType = new TypeToken<ArrayList<DatabaseConfig>>() {}.getType();
			configs = gson.fromJson(reader, collectionType);
			
		} else {
			configFile.createNewFile();
		}
		
		if(configs == null) { //If there are no Entries in the JSON file, configs could be set to null
			configs = new ArrayList<DatabaseConfig>();
		}
		
		this.configFile = configFile;
	}
	
	public void updateConfig(String ConfigName, String DBtype, String DBurl, String DBuser, String DBpassword) throws IOException{
		boolean alreadyExistingConfig = false;
		for(DatabaseConfig config : configs){
			if(config.getConfigName().equals(ConfigName)){
				alreadyExistingConfig = true;
				config.setDBtype(DBtype);
				config.setDBurl(DBurl);
				config.setDBuser(DBuser);
				config.setDBpassword(DBpassword);
			}
		}
		if (!alreadyExistingConfig){
			configs.add(new DatabaseConfig(ConfigName, DBtype, DBurl, DBuser, DBpassword));
		}
		save();
	}
	
	public void removeConfig(int index) throws IOException{
		configs.remove(index);
		save();
	}
	
	public String[] getConfigNames(){
		ArrayList<String> configNames = new ArrayList<String>();
		for (DatabaseConfig config : configs){
			configNames.add(config.getConfigName());
		}
		String[] configNamesArray = new String[configNames.size()];
		return configNames.toArray(configNamesArray);
	}
	
	public void save() throws IOException{
		FileWriter writer = new FileWriter(configFile);
		writer.write(gson.toJson(configs));
		writer.close();
	}
	
	public DatabaseConfig getConfig(String ConfigName){
		for (DatabaseConfig config : configs) {
			if (config.getConfigName() == ConfigName)
				return config;
		}
		return null;
	}
	
}
