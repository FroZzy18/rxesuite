package org.processmining.plugins.rxesuite;
import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseHandler {
	
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	public DatabaseHandler(){}
	
	public DatabaseHandler(String url, String username, String password, String driver, String dbName) throws Exception {
		switch (driver) {
			case "PostgreSQL":		Class.forName("org.postgresql.Driver");
									url = "jdbc.postgresql://".concat(url);
									break;
									
			case "Oracle":			Class.forName("oracle.jdbc.OracleDriver");
									url = "jdbc:oracle:thin:@".concat(url);
									break;
									
			case "MicrosoftSQL":	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
									url = "jdbc:sqlserver://".concat(url);
									break;
									
			case "SQLite":			Class.forName("org.sqlite.JDBC");
									url = "jdbc:sqlite:".concat(url);
									break;
									
			default:				Class.forName("com.mysql.jdbc.Driver");
									url = "jdbc:mysql://".concat(url);
									break;
		}
		connect = DriverManager.getConnection(url,username,password);
		statement = connect.createStatement();
		statement.executeUpdate("CREATE DATABASE IF NOT EXISTS "+ dbName +";");
		statement.executeQuery("use "+ dbName +";");
		
	}
	
	public void rxesSetup() throws Exception{
		statement.executeUpdate("DROP TABLE IF EXISTS "
				+ "event_has_attribute, "
				+ "trace_has_attribute, "
				+ "log_has_attribute, "
				+ "trace_has_event, "
				+ "log_has_trace, "
				+ "attribute, "
				+ "classifier, "
				+ "event, "
				+ "trace, "
				+ "log, "
				+ "extension, "
				+ "event_collection,"
				+ "log_flat, "
				+ "declareconstraints"
				+ ";");
		
		statement.executeUpdate("CREATE TABLE log"
				+ "("
				+ "id BINARY(16) NOT NULL, "
				+ "name TEXT, "
				+ "CONSTRAINT pk_log PRIMARY KEY (id)"
				+ ");");
		
		statement.executeUpdate("CREATE TABLE trace "
				+ "("
				+ "id BINARY(16), "
				+ "CONSTRAINT pk_trace PRIMARY KEY (id)"
				+ ");");
		
		statement.executeUpdate("CREATE TABLE event_collection "
				+ "("
				+ "id BINARY(16), "
				+ "name VARCHAR(45), "
				+ "CONSTRAINT pk_event_collection PRIMARY KEY (id) "
				+ ");");
		
		statement.executeUpdate("CREATE TABLE event "
				+ "("
				+ "id BINARY(16), "
				+ "event_collection_id BINARY(16), "
				+ "CONSTRAINT pk_event PRIMARY KEY (id), "
				+ "CONSTRAINT fk_event_event_collection FOREIGN KEY (event_collection_id) REFERENCES event_collection (id)"
				+ ");");
		
		statement.executeUpdate("CREATE TABLE extension "
				+ "("
				+ "id BINARY(16), "
				+ "name VARCHAR(45), "
				+ "prefix VARCHAR(45), "
				+ "uri TEXT, "
				+ "CONSTRAINT pk_extension PRIMARY KEY (id) "
				+ ");");
		
		statement.executeUpdate("CREATE TABLE attribute "
				+ "("
				+ "id BINARY(16), "
				+ "attr_key TEXT, "
				+ "attr_type TEXT, "
				+ "parent_id BINARY(16), "
				+ "extension_id BINARY(16), "
				+ "CONSTRAINT pk_attribute PRIMARY KEY (id),"
				+ "CONSTRAINT fk_attribute_attribute FOREIGN KEY (parent_id) REFERENCES attribute (id), "
				+ "CONSTRAINT fk_attribute_extension FOREIGN KEY (extension_id) REFERENCES extension (id) "
				+ ");");
		
		statement.executeUpdate("CREATE TABLE classifier "
				+ "("
				+ "id BINARY(16), "
				+ "name VARCHAR(45), "
				+ "attr_keys TEXT, "
				+ "log_id BINARY(16), "
				+ "CONSTRAINT pk_classifier PRIMARY KEY (id), "
				+ "CONSTRAINT fk_classifier_log FOREIGN KEY (log_id) REFERENCES log (id) "
				+ ");");
		
		statement.executeUpdate("CREATE TABLE log_has_attribute "
				+ "("
				+ "log_id BINARY(16), "
				+ "trace_global BIT, "
				+ "event_global BIT, "
				+ "attribute_id BINARY(16), "
				+ "value TEXT, "
				+ "CONSTRAINT pk_log_has_attribute PRIMARY KEY(log_id, trace_global, event_global, attribute_id), "
				+ "CONSTRAINT fk_log_has_attribute_log FOREIGN KEY (log_id) REFERENCES log (id), "
				+ "CONSTRAINT fk_log_has_attribute_attribute FOREIGN KEY (attribute_id) REFERENCES attribute (id) "
				+ ");");
		
		statement.executeUpdate("CREATE TABLE log_has_trace "
				+ "("
				+ "log_id BINARY(16), "
				+ "trace_id BINARY(16), "
				+ "sequence BIGINT, "
				+ "CONSTRAINT pk_log_has_trace PRIMARY KEY(log_id, trace_id), "
				+ "CONSTRAINT fk_log_has_trace_log FOREIGN KEY (log_id) REFERENCES log (id), "
				+ "CONSTRAINT fk_log_has_trace_trace FOREIGN KEY (trace_id) REFERENCES trace (id) "
				+ ");");
		
		statement.executeUpdate("CREATE TABLE trace_has_attribute "
				+ "("
				+ "trace_id BINARY(16), "
				+ "attribute_id BINARY(16), "
				+ "value TEXT, "
				+ "CONSTRAINT pk_trace_has_attribute PRIMARY KEY(trace_id, attribute_id), "
				+ "CONSTRAINT fk_trace_has_attribute_trace FOREIGN KEY (trace_id) REFERENCES trace (id), "
				+ "CONSTRAINT fk_trace_has_attribute_attribute FOREIGN KEY (attribute_id) REFERENCES attribute (id) "
				+ ");");
		
		statement.executeUpdate("CREATE TABLE trace_has_event "
				+ "("
				+ "trace_id BINARY(16), "
				+ "event_id BINARY(16), "
				+ "sequence BIGINT, "
				+ "CONSTRAINT pk_trace_has_event PRIMARY KEY(trace_id, event_id), "
				+ "CONSTRAINT fk_trace_has_event_trace FOREIGN KEY (trace_id) REFERENCES trace (id), "
				+ "CONSTRAINT fk_trace_has_event_event FOREIGN KEY (event_id) REFERENCES event (id) "
				+ ");");
		
		statement.executeUpdate("CREATE TABLE event_has_attribute "
				+ "("
				+ "event_id BINARY(16), "
				+ "attribute_id BINARY(16), "
				+ "value TEXT, "
				+ "CONSTRAINT pk_event_has_attribute PRIMARY KEY(event_id, attribute_id), "
				+ "CONSTRAINT fk_event_has_attribute_event FOREIGN KEY (event_id) REFERENCES event (id), "
				+ "CONSTRAINT fk_event_has_attribute_attribute FOREIGN KEY (attribute_id) REFERENCES attribute (id) "
				+ ");");
		
		statement.executeUpdate("CREATE TABLE log_flat "
				+ "("
				+ "EventID BINARY(16), "
				+ "Instance BINARY(16), "
				+ "Task VARCHAR(200), "
				+ "Time BIGINT, "
				+ "CONSTRAINT pk_log_flat PRIMARY KEY(EventID)"
				+ ");");
		
		statement.executeUpdate("CREATE TABLE declareconstraints"
				+ "("
				+ "ConstraintName VARCHAR(100),"
				+ "TaskA VARCHAR(100),"
				+ "TaskB VARCHAR(100),"
				+ "Support FLOAT,"
				+ "Confidence FLOAT,"
				+ "CONSTRAINT pk_declare PRIMARY KEY(ConstraintName, TaskA, TaskB)"
				+ ");");
		
		System.out.println("Database structure created successfully!");
		
	}
	
	public int update(String update) throws SQLException {
		return statement.executeUpdate(update);
	}
	
	public ResultSet query(String query) throws SQLException {
		return statement.executeQuery(query);
	}
	
	public int insertLog(String id, String name) throws SQLException {
		return statement.executeUpdate("INSERT INTO log (id, name) VALUES ("
				+ "UNHEX('" + id + "'), "
				+ "'" + name + "'" 
				+ ");");
	}

	public int insertTrace(String id) throws SQLException {
		return statement.executeUpdate("INSERT INTO trace (id)  VALUES ("
				+"UNHEX('"+ id +"'));");
	}
	
	public int insertEvent(String id) throws SQLException {
		return statement.executeUpdate("INSERT INTO event (id)  VALUES ("
				+"UNHEX('"+ id +"'));");
	}
	
	public int insertLog_has_Trace(String logId, String traceId, long sequence) throws SQLException{
		return statement.executeUpdate("INSERT INTO log_has_trace (log_id, trace_id, sequence) VALUES ("
				+ "UNHEX('" +logId +"'), " 
				+ "UNHEX('" +traceId +"'), " 
				+ "'" + sequence + "' "
				+ ");");
	}
	
	public int insertTrace_has_Event(String traceId, String eventId, long sequence) throws SQLException{
		return statement.executeUpdate("INSERT INTO trace_has_event (trace_id, event_id, sequence) VALUES ("
				+ "UNHEX('" +traceId +"'), " 
				+ "UNHEX('" +eventId +"'), " 
				+ "'" + sequence + "' "
				+ ");");
	}
	
	public int insertExtension(String id, String name, String prefix, URI uri) throws SQLException{
		return statement.executeUpdate("INSERT INTO extension (id, name, prefix, uri) VALUES ("
				+ "UNHEX('" + id  +"'), "  
				+ "'" + name + "', "
				+ "'" + prefix + "', "
				+ "'" + uri.toString() + "' "
				+ ");");
	}
	
	public int insertClassifier(String id, String name, String attr_keys, String log_id) throws SQLException{
		return statement.executeUpdate("INSERT INTO classifier (id, name, attr_keys, log_id) VALUES ("
				+ "UNHEX('" + id  +"'), "  
				+ "'" + name + "', "
				+ "'" + attr_keys + "', "
				+ "UNHEX('" + log_id  +"')"
				+ ");");
	}
	
	public int insertAttribute(String id, String attr_key, String attr_type, String parent_id, String extension_id) throws SQLException{
		StringBuilder update = new StringBuilder();
		update.append("INSERT INTO attribute (id, attr_key, attr_type, parent_id, extension_id) VALUES ("
				+ "UNHEX('" + id  +"'), "  
				+ "'" + attr_key + "', "
				+ "'" + attr_type + "', ");
		
		if(parent_id != null) update.append("UNHEX('" + parent_id  +"'), ");
		else update.append("NULL, ");
		
		if(extension_id != null)update.append("UNHEX('" + extension_id  +"')");
		else update.append("NULL");
		
		update.append(");");
		
		return statement.executeUpdate(update.toString());
	}
	
	public int insertTrace_has_Attribute(String trace_id, String attribute_id, String value) throws SQLException{
		return statement.executeUpdate("INSERT INTO trace_has_attribute (trace_id, attribute_id, value) VALUES ("
				+ "UNHEX('" + trace_id  +"'), "   
				+ "UNHEX('" + attribute_id  +"'), "
				+ "'" + value + "'"
				+ ");");
	}
	
	public int insertEvent_has_Attribute(String event_id, String attribute_id, String value) throws SQLException{
		return statement.executeUpdate("INSERT INTO event_has_attribute (event_id, attribute_id, value) VALUES ("
				+ "UNHEX('" + event_id  +"'), "   
				+ "UNHEX('" + attribute_id  +"'), "
				+ "'" + value + "'"
				+ ");");
	}
	
	public int insertLog_has_Attribute(String log_id, boolean trace_global, boolean event_global, String attribute_id, String value) throws SQLException{
		return statement.executeUpdate("INSERT INTO log_has_attribute (log_id, trace_global, event_global, attribute_id, value) VALUES ("
				+ "UNHEX('" + log_id  +"'), "
				+ trace_global + ", "
				+ event_global + ", "				
				+ "UNHEX('" + attribute_id  +"'), "
				+ "'" + value + "'"
				+ ");");
	}
	
	public String getExtensionID(String name, String prefix, URI uri) throws SQLException {
		resultSet = statement.executeQuery("select HEX(id) from extension where "
				+ "name = '"+name+"' AND "
				+ "prefix ='"+prefix+"' AND "
				+ "uri = '"+uri.toString()+"';");
		
		if(resultSet.next()) 
			return resultSet.getString(1);
		
		return null;
	}

	public Map<String, String> getAttributeMap() throws SQLException{
		Map<String, String> attributeMap = new HashMap<String, String>();
		resultSet = statement.executeQuery("select HEX(id), attr_key, attr_type from attribute;");
		while(resultSet.next()){
			attributeMap.put(resultSet.getString(2)+" "+resultSet.getString(3), resultSet.getString(1));
		}
		return attributeMap;
	}
	
	public String getLogId(String LogName) throws SQLException{
		resultSet = statement.executeQuery("select HEX(id) from log where name = '" + LogName + "';");
		
		if (resultSet.next())
			return resultSet.getString(1);
		
		return null;
	}
	
	public String getClassifierId(String name, String attr_keys, String log_id) throws SQLException{
		resultSet = statement.executeQuery("select HEX(id) from classifier where "
				+ "name ='"+ name +"' AND "
				+ "attr_keys = '" + attr_keys + "' AND "
				+ "log_id = UNHEX('" + log_id +"');");
		
		if (resultSet.next())
			return resultSet.getString(1);
		
		return null;
	}
	
	public void disableChecks() {
		try {statement.executeUpdate("SET "
				+ "autocommit = 0, "
				+ "unique_checks = 0,"
				+ "foreign_key_checks = 0;");
		} catch (SQLException e) {
			System.out.println("Disable Check Feature not available.");
		}
	}
	
	public void enableChecks(){
		try {statement.executeUpdate("SET "
				+ "autocommit = 1, "
				+ "unique_checks = 1,"
				+ "foreign_key_checks = 1;");
		} catch (SQLException e) {
			System.out.println("Disable Check Feature not available.");
		}
	}
	
	public int populateLogFlat(String TaskFieldName) throws SQLException{
		return statement.executeUpdate("INSERT INTO log_flat SELECT "
				+ "the.event_id, "
				+ "trace_id, "
				+ "eha.value, "
				+ "the.sequence from trace_has_event the "
				+ "INNER JOIN event e on the.event_id=e.id "
				+ "INNER JOIN event_has_attribute eha ON e.id = eha.event_id "
				+ "INNER JOIN attribute a ON a.id = eha.attribute_id "
				+ "WHERE a.attr_key like '"+TaskFieldName+"';");
	}
	
	// ALL derive...() functions were originally only designed for MSSQL. Probably need some reworking to also work with MySQL.
	
	public int deriveResponseConstraints() throws SQLException {
		return statement.executeUpdate("INSERT INTO declare "
				+ "SELECT 'Response', TaskA, TaskB,(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) AS Support, "
				+ "((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskA GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence "
				+ "FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x "
				+ "WHERE a.Task = x.TaskA AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskB AND b.Instance = a.Instance AND b.Time > a.Time) "
				+ "GROUP BY x.TaskA, x.TaskB;");
	}
	
	public int deriveAlternateResponseConstraints() throws SQLException {
		return statement.executeUpdate("INSERT INTO declare "
				+ "SELECT 'AlternateResponse', TaskA, TaskB, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) AS Support, "
				+ "((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskA GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence "
				+ "FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x "
				+ "WHERE a.Task = x.TaskA AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskB AND b.Instance = a.Instance AND b.Time > a.Time) "
				+ "AND NOT EXISTS(SELECT *  FROM log_flat b, log_flat c WHERE c.Instance = a.Instance AND c.Task = x.TaskA AND b.Instance = a.Instance AND b.Task = x.TaskB AND c.Time > a.Time AND c.Time < b.Time) "
				+ "GROUP BY x.TaskA, x.TaskB;");
	}
	
	public int deriveChainResponseConstraints() throws SQLException {
		return statement.executeUpdate("INSERT INTO declare "
				+ "SELECT 'ChainResponse', TaskA, TaskB, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) AS Support, "
				+ "((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskA GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence "
				+ "FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x "
				+ "WHERE a.Task = x.TaskA AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskB AND b.Instance = a.Instance AND b.Time > a.Time) "
				+ "AND NOT EXISTS(SELECT * FROM log_flat b, log_flat c WHERE c.Instance = a.Instance AND b.Instance = a.Instance AND b.Task = x.TaskB AND c.Time > a.Time AND c.Time < b.Time) "
				+ "GROUP BY x.TaskA, x.TaskB;"); 
	}
	
	public int derivePrecedenceConstraints() throws SQLException {
		return statement.executeUpdate("INSERT INTO declare "
				+ "SELECT 'Precedence', TaskA, TaskB, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskB) AS FLOAT)) AS Support, "
				+ "((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskB) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskB GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence "
				+ "FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x "
				+ "WHERE a.Task = x.TaskB AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskA AND b.Instance = a.Instance AND b.Time < a.Time) "
				+ "GROUP BY x.TaskA, x.TaskB;");
	}
	
	public int deriveAlternatePrecedenceConstraints() throws SQLException {
		return statement.executeUpdate("INSERT INTO declare "
				+ "SELECT 'AlternatePrecedence', TaskA, TaskB, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskB) AS FLOAT)) AS Support, "
				+ "((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskB) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskB GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence "
				+ "FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x "
				+ "WHERE a.Task = x.TaskB AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskA AND b.Instance = a.Instance AND b.Time < a.Time) "
				+ "AND NOT EXISTS(SELECT *  FROM log_flat b, log_flat c WHERE c.Instance = a.Instance AND c.Task = x.TaskB AND b.Instance = a.Instance AND b.Task = x.TaskA AND c.Time < a.Time AND c.Time > b.Time) "
				+ "GROUP BY x.TaskA, x.TaskB;");
	}
	
	public int deriveChainPrecedenceConstraints() throws SQLException {
		return statement.executeUpdate("INERT INTO declare "
				+ "SELECT 'ChainPrecedence', TaskA, TaskB, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskB) AS FLOAT)) AS Support, "
				+ "((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskB) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskB GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence "
				+ "FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x "
				+ "WHERE a.Task = x.TaskB AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskA AND b.Instance = a.Instance AND b.Time < a.Time) "
				+ "AND NOT EXISTS(SELECT *  FROM log_flat b, log_flat c WHERE c.Instance = a.Instance AND b.Instance = a.Instance AND b.Task = x.TaskA AND c.Time < a.Time AND c.Time > b.Time) "
				+ "GROUP BY x.TaskA, x.TaskB;");
	}
	
	public int deriveRespondedExistanceConstraints() throws SQLException {
		return statement.executeUpdate("INSERT INTO declare "
				+ "SELECT 'RespondedExistence', TaskA, TaskB, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) AS Support, "
				+ "((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskA GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence "
				+ "FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x "
				+ "WHERE a.Task = x.TaskB AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskA AND b.Instance = a.Instance) "
				+ "GROUP BY x.TaskA, x.TaskB;");
	}
	
	public int deriveNotSuccessionConstraints() throws SQLException {
		return statement.executeUpdate("INSERT INTO declare "
				+ "SELECT 'NotSuccession', TaskA, TaskB, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) AS Support, "
				+ "((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskA GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence "
				+ "FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x "
				+ "WHERE a.Task = x.TaskB AND a.Time < ALL (SELECT Time FROM log_flat b WHERE b.Task = x.TaskA AND b.Instance = a.Instance) "
				+ "AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskA AND b.Instance = a.Instance) "
				+ "AND a.Time > ALL(SELECT Time FROM log_flat b WHERE b.Task = x.TaskB AND b.Instance = a.Instance) "
				+ "GROUP BY x.TaskA, x.TaskB;");
	}

	public int deriveLength2LoopConstraints() throws SQLException {
		return statement.executeUpdate("INSERT INTO declare "
				+ "select 'Lenght_2_Loop', a.Task as 'TaskA', b.Task as 'TaskB', "
				+ "COUNT(*) / CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE a.Task) AS FLOAT)  as 'Support', "
				+ "COUNT(*) / CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE a.Task) AS FLOAT) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE a.Task GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT)) AS Confidence "
				+ "FROM log_flat a, log_flat b, log_flat c "
				+ "WHERE a.Instance = b.Instance AND b.Instance = c.Instance "
				+ "AND a.Task = c.Task AND b.Time = a.Time+1 AND c.Time = b.Time +1 "
				+ "GROUP BY a.Task, b.Task;");
	}
	
	public int deriveInitConstraints() throws SQLException {
		statement.executeUpdate("DELETE FROM declareconstraints WHERE ConstraintName = 'Init';");
		
		return statement.executeUpdate("INSERT INTO declareconstraints "
				+ "select 'Init', Task, Task, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(distinct Instance) FROM log_flat) AS FLOAT)) AS Support, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(distinct Instance) FROM log_flat) AS FLOAT)) * (CAST((SELECT COUNT(distinct Instance) FROM log_flat i WHERE i.Task = o.Task ) AS FLOAT)) / CAST((SELECT COUNT(distinct Instance) FROM log_flat) AS FLOAT) AS Confidence	 "
				+ "FROM log_flat o "
				+ "WHERE o.Time = 1 "
				+ "GROUP BY Task;");
	}
	
	public int deriveEndConstraints() throws SQLException {
		statement.executeUpdate("DELETE FROM declareconstraints WHERE ConstraintName = 'End';");
		
		return statement.executeUpdate("INSERT INTO declareconstraints "
				+ "select 'End', Task, Task, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(distinct Instance) FROM log_flat) AS FLOAT)) AS Support, "
				+ "(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(distinct Instance) FROM log_flat) AS FLOAT)) * (CAST((SELECT COUNT(distinct Instance) FROM log_flat i WHERE i.Task = o.Task ) AS FLOAT)) / CAST((SELECT COUNT(distinct Instance) FROM log_flat) AS FLOAT) AS Confidence	 "
				+ "FROM log_flat o "
				+ "WHERE o.Time = (SELECT MAX(i2.Time) from log_flat i2 WHERE o.Instance = i2.Instance) "
				+ "GROUP BY Task;");
	}
	
	public String[][] getDeclareConstraints() throws SQLException {
		resultSet = statement.executeQuery("SELECT COUNT(*) from declareconstraints");
		int size = resultSet.getInt(0);
		String[][] declareConstraints = new String[size][5];
		resultSet = statement.executeQuery("SELECT * from declareconstraints");
		int counter = 0;
		while(resultSet.next()) {
			declareConstraints[counter][0] = resultSet.getString(0);
			declareConstraints[counter][1] = resultSet.getString(1);
			declareConstraints[counter][2] = resultSet.getString(2);
			declareConstraints[counter][3] = resultSet.getString(3);
			declareConstraints[counter][4] = resultSet.getString(4);
			counter++;
		}
		return declareConstraints;
	}
	
	public String[][] getDirectSuccessorRelations() throws SQLException {
		resultSet = statement.executeQuery("SELECT DISTINCT COUNT(*) from declareconstraints WHERE ConstraintNane = 'ChainResponse';");
		resultSet.next();
		int size = resultSet.getInt(1);
		String[][] directSuccessorRelations = new String[size][2];
		resultSet = statement.executeQuery("SELECT DISTINCT TaskA, TaskB from declareconstraints"
											+ "WHERE ConstraintName = 'ChainResponse';");
		int counter = 0;
		while(resultSet.next()) {
			directSuccessorRelations[counter][0] = resultSet.getString(1);
			directSuccessorRelations[counter][1] = resultSet.getString(2);
			counter++;
		}
		return directSuccessorRelations;
	}
	
	public String[][] getLength2LoopRelations() throws SQLException {
		resultSet = statement.executeQuery("SELECT DISTINCT COUNT(*) from declareconstraints WHERE ConstraintName = 'Length_2_Loop';");
		resultSet.next();
		int size = resultSet.getInt(1);
		String[][] length2LoopRelations = new String[size][2];
		resultSet = statement.executeQuery("SELECT DISTINCT TaskA, TaskB from declareconstraints "
											+ "WHERE ConstraintName = 'Length_2_Loop';");
		int counter = 0;
		while(resultSet.next()) {
			length2LoopRelations[counter][0] = resultSet.getString(1);
			length2LoopRelations[counter][1] = resultSet.getString(2);
			counter++;
		}
		return length2LoopRelations;
	}
	
	public String[][] getLongDistanceDependencyRelations() throws SQLException {
		resultSet = statement.executeQuery("SELECT DISTINCT COUNT(*) from declareconstraints WHERE ConstraintName = 'Response';");
		resultSet.next();
		int size = resultSet.getInt(1);
		String[][] longDistanceDependencyRelations = new String[size][2];
		resultSet = statement.executeQuery("SELECT DISTINCT TaskA, TaskB from declareconstraints "
											+ "WHERE ConstraintName = 'Response';");
		int counter = 0;
		while(resultSet.next()) {
			longDistanceDependencyRelations[counter][0] = resultSet.getString(1);
			longDistanceDependencyRelations[counter][1] = resultSet.getString(2);
			counter++;
		}
		return longDistanceDependencyRelations;
	}
	
	public String[] getStartingActivities() throws SQLException {
		resultSet = statement.executeQuery("SELECT DISTINCT COUNT(*) from declareconstraints WHERE ConstraintName = 'Init';");
		resultSet.next();
		int size = resultSet.getInt(1);
		String[] startingActivities = new String[size];
		resultSet = statement.executeQuery("SELECT DISTINCT TaskA from declareconstraints "
											+ "WHERE ConstraintName = 'Init';");
		int counter = 0;
		while(resultSet.next()) {
			startingActivities[counter] = resultSet.getString(1);
			counter++;
		}
		return startingActivities;
	}
	
	public String[] getEndingActivities() throws SQLException {
		resultSet = statement.executeQuery("SELECT DISTINCT COUNT(*) from declareconstraints WHERE ConstraintName = 'End';");
		resultSet.next();
		int size = resultSet.getInt(1);
		String[] endingActivities = new String[size];
		resultSet = statement.executeQuery("SELECT DISTINCT TaskA from declareconstraints "
											+ "WHERE ConstraintName = 'End';");
		int counter = 0;
		while(resultSet.next()) {
			endingActivities[counter] = resultSet.getString(1);
			counter++;
		}
		return endingActivities;
	}
	
	public ArrayList<ArrayList<String>> getTraces() throws SQLException {
		resultSet = statement.executeQuery("SELECT Instance, Task, Time from log_flat ORDER BY Instance, Time");
		ArrayList<ArrayList<String>> traces = new ArrayList<ArrayList<String>>();
		String currentInstance = "";
		int counter = -1;
		
		while(resultSet.next()){
			if (currentInstance.equals(resultSet.getString(1))){
				traces.get(counter).add(resultSet.getString(2));
			} else {
				currentInstance = resultSet.getString(1);
				counter++;
				traces.add(new ArrayList<String>());
				traces.get(counter).add(resultSet.getString(2));
			}
		}
		return traces;
	}
	
	
	
	protected void finalize(){
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (preparedStatement != null){
				preparedStatement.close();
			}
			if (connect != null) {
				connect.close();
			}
		} catch (SQLException ex) {
			Logger lgr = Logger.getLogger(DatabaseHandler.class.getName());
			lgr.log(Level.WARNING, ex.getMessage(), ex);
		}
	}


}
