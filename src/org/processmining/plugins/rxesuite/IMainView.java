package org.processmining.plugins.rxesuite;

public interface IMainView {
	public void log(String s);
	public void processFinished();
	public void enableStopButton();
	public void setProgressBarProgress(int progress);
}
