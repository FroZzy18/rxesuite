package org.processmining.plugins.rxesuite;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;

import com.google.common.base.CaseFormat;

public class MainView implements IMainView{

	private IRXESPresenter presenter;
	private ConfigHandler ch;
	
	final static String MainTab = "Import";
	final static String DBConfigTab = "Database Configuration";
	final static String ConstraintsTab = "Main";
	final static String[] supportedMiners = {"Declare","\u03B1-Algorithm","\u03B1+-Algorithm","\u03B1++-Algorithm","Heuristics-Miner"};
	
	private DefaultTableModel declareConstraintsModel = new DefaultTableModel(null,new String[]{"Constraint","Support","Confidence"});
	private DefaultTableModel alphaModel = new DefaultTableModel(null,new String[]{"TaskA","RelationSymbol","TaskB"});
	private DefaultTableModel alphapModel = new DefaultTableModel(null,new String[]{"TaskA","RelationSymbol","TaskB"});
	private DefaultTableModel alphappModel = new DefaultTableModel(null,new String[]{"TaskA","RelationSymbol","TaskB"});
	private DefaultTableModel heuristicsModel = new DefaultTableModel(null,new String[]{"TaskA","RelationSymbol","TaskB"});
	
	private JFrame frame = new JFrame("RXESuite");
	private JTabbedPane tabbedPane = new JTabbedPane();
	
	// Components for the Constraints/Relation Tab
	private JPanel constraintsTabPanel = new JPanel();
	private JPanel constraintsTabDatabaseNamePanel = new JPanel();
	private JLabel constraintsTabDatabaseNameLabel = new JLabel("Database Name:");
	private JTextField constraintsTabDatabaseNameTextField = new JTextField(50);
	private JTable dataTable = new JTable();
	private JScrollPane dataTableScrollPane;
	private TitledBorder tableBorder = BorderFactory.createTitledBorder("Constraints / Relations");
	private JButton deriveConstraintsButton = new JButton("Derive Constraints and Relations");
	private JPanel minerSection = new JPanel();
	private JComboBox minersComboBox;
	private JPanel thresholds = new JPanel();
	private TitledBorder thresholdsTitleBorder = BorderFactory.createTitledBorder("Thresholds for Declare");
	private JLabel supportThresholdLabel = new JLabel("Support: ");
	private JTextField supportThresholdTextField = new JTextField("0.0");
	private JLabel confidenceThresholdLabel = new JLabel("Confidence: ");
	private JTextField confidenceThresholdTextField = new JTextField("0.0");
	private JPanel activityFieldNamePanel = new JPanel();
	private TitledBorder activityFieldNameBorder = BorderFactory.createTitledBorder("Activity Field");
	private JLabel activityFieldNameLabel = new JLabel("Name of Activity Field: ");
	private JTextField activityFieldNameTextField = new JTextField("concept:name");
	private JPanel exportButtons = new JPanel();
	private TitledBorder exportButtonsBorder = BorderFactory.createTitledBorder("Export Constraints/Relations");
	private JButton exportDeclareButton = new JButton("Export Declare Constraints");
	private JButton exportAlphaButton = new JButton("Export Alpha-Algorithm Relations");
	private JButton exportAlphapButton = new JButton("Export Alpha+-Algorithm Relations");
	private JButton exportAlphappButton = new JButton("Export Alpha++-Algorithm Relations");
	private JButton exportHeuristicsButton = new JButton("Export Heuristics-Miner Relations");
	
	// Components for the Import/Main Tab
	private JPanel main = new JPanel();
	private JPanel top = new JPanel();
	private JPanel fileHandling = new JPanel();
	private JPanel insertHandling = new JPanel();
	private JPanel control = new JPanel();
	private JTextField filenameTextfield = new JTextField(50);
	private JRadioButton newDatabaseRadioButton = new JRadioButton("Create new Database");
	private JRadioButton updateDatabaseRadioButton = new JRadioButton("Update existing Database");
	private ButtonGroup insertHandlingButtonGroup = new ButtonGroup();
	private JLabel insertLabel = new JLabel("Choose Insert Method:");
	private JLabel seperatorLabel = new JLabel("|");
	private JLabel databaseNameLabel = new JLabel("Database Name:");
	private JTextField databaseNameTextField = new JTextField(20);
	private JButton selectFileButton = new JButton("File...");
	private JTextArea statusTextArea = new JTextArea(20,50);
	private JScrollPane scroll;
	private JButton startButton = new JButton("Start Import");
	private JButton stopButton = new JButton("Stop Import");
	private JProgressBar progressBar = new JProgressBar(0);
	
	// Components for the Database Config Tab
	private JPanel configureDB = new JPanel();
	private GridBagConstraints gbc = new GridBagConstraints();
	private JLabel savedConfigsLabel = new JLabel("Saved Configs:", SwingConstants.CENTER);
	private JComboBox savedConfigs;
	private JLabel savedConfigNameLabel = new JLabel("Config-Name:", SwingConstants.CENTER);
	private JTextField savedConfigNameTextField = new JTextField(30);
	private JLabel dbTypeLabel = new JLabel("DB Type:", SwingConstants.CENTER);
	private JComboBox dbTypeComboBox;
	private JLabel dbURLLabel = new JLabel("URL / Path:", SwingConstants.CENTER);
	private JTextField dbURLTextField = new JTextField(30);
	private JLabel dbUserNameLabel = new JLabel("Username:", SwingConstants.CENTER);
	private JTextField dbUserNameTextField = new JTextField(30);
	private JLabel dbPasswordLabel = new JLabel("Password:", SwingConstants.CENTER);
	private JTextField dbPasswordTextField = new JTextField(30);
	private JButton saveNewConfigButton = new JButton("Save Config");
	private JButton deleteConfigButton = new JButton("Delete Selected Config");
	
	public MainView() throws IOException {
		ch = new ConfigHandler(new File("config.json"));
		createUI();
		showDeclareConstraints();
	}
	
	private void createUI(){
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(700,400));
		
		// Constraints/Relations Tab
		constraintsTabPanel.setLayout(new BorderLayout());
		
		constraintsTabDatabaseNamePanel.add(constraintsTabDatabaseNameLabel);
		constraintsTabDatabaseNamePanel.add(constraintsTabDatabaseNameTextField);
		constraintsTabDatabaseNameTextField.setHorizontalAlignment(JTextField.CENTER);
		
		dataTable.getTableHeader().setReorderingAllowed(false);
		dataTable.setDefaultEditor(Object.class, null);
		dataTableScrollPane = new JScrollPane(dataTable);
		dataTableScrollPane.setBorder(tableBorder);
		
		minersComboBox = new JComboBox(supportedMiners);
		minersComboBox.addActionListener((e) -> {		
				switch((String) minersComboBox.getSelectedItem()) {
				case "Declare":
					showDeclareConstraints();
					break;
				case "\u03B1-Algorithm":
					showAlphaRelations();
					break;
				case "\u03B1+-Algorithm":
					showAlphapRelations();
					break;
				case "\u03B1++-Algorithm":
					showAlphappRelations();
					break;
				case "Heuristics-Miner":
					showHeuristicsRelations();
					break;
				default:
					System.out.println("ComboBox Error!");
				}
		});
		
		thresholds.setLayout(new GridLayout(2,2));
		thresholds.add(supportThresholdLabel);
		thresholds.add(supportThresholdTextField);
		thresholds.add(confidenceThresholdLabel);
		thresholds.add(confidenceThresholdTextField);
		thresholds.setBorder(thresholdsTitleBorder);
		
		activityFieldNamePanel.setLayout(new GridLayout(1,2));
		activityFieldNamePanel.add(activityFieldNameLabel);
		activityFieldNamePanel.add(activityFieldNameTextField);
		activityFieldNamePanel.setBorder(activityFieldNameBorder);
		
		//minerSection.setLayout(new GridLayout(3,1));
		minerSection.setLayout(new BoxLayout(minerSection, BoxLayout.Y_AXIS));
		minerSection.add(minersComboBox);
		minerSection.add(Box.createVerticalStrut(220));
		minerSection.add(thresholds);
		minerSection.add(activityFieldNamePanel);
		
		exportDeclareButton.setEnabled(false);
		exportAlphaButton.setEnabled(false);
		exportAlphapButton.setEnabled(false);
		exportAlphappButton.setEnabled(false);
		exportHeuristicsButton.setEnabled(false);
		
		exportDeclareButton.addActionListener((e) -> exportTableModelAsCSV(declareConstraintsModel, "declare_constraints.csv"));
		exportAlphaButton.addActionListener((e) -> exportTableModelAsCSV(alphaModel, "alpha_relations.csv"));
		exportAlphapButton.addActionListener((e) -> exportTableModelAsCSV(alphapModel, "alpha+_relations.csv"));
		exportAlphappButton.addActionListener((e) -> exportTableModelAsCSV(alphappModel, "alpha++_relations.csv"));
		exportHeuristicsButton.addActionListener((e) -> exportTableModelAsCSV(heuristicsModel, "heuristics_miner_relations.csv"));
		
		exportButtons.setBorder(exportButtonsBorder);
		exportButtons.setLayout(new GridLayout(5,1));
		exportButtons.add(exportDeclareButton);
		exportButtons.add(exportAlphaButton);
		exportButtons.add(exportAlphapButton);
		exportButtons.add(exportAlphappButton);
		exportButtons.add(exportHeuristicsButton);
		
		deriveConstraintsButton.addActionListener((e) -> {
			if (constraintsTabDatabaseNameTextField.getText().isEmpty() || activityFieldNameTextField.getText().isEmpty()) {
				String errorMessage = "";
				if (constraintsTabDatabaseNameTextField.getText().isEmpty())
					errorMessage += "Database-Name is missing! \n";
				if(activityFieldNameTextField.getText().isEmpty())
					errorMessage += "Activity-Field-Name is missing!";
				JOptionPane.showMessageDialog(frame, errorMessage, "Error!", JOptionPane.WARNING_MESSAGE);
			} else {
				String[] declarativeConstraints = {"Response", "AlternateResponse", "ChainResponse",
													"Precedence", "AlternatePrecedence", "ChainPrecedence",
													"RespondedExistance", "NotSuccession", "Length_2_Loop",
													"Init", "End"};
				try {
					presenter.deriveDeclarativeConstraints(declarativeConstraints,
															activityFieldNameTextField.getText(),
															dbURLTextField.getText(), 
															dbUserNameTextField.getText(), 
															dbPasswordTextField.getText(),
															(String) dbTypeComboBox.getSelectedItem(),
															constraintsTabDatabaseNameTextField.getText());
					exportDeclareButton.setEnabled(true);
					exportAlphaButton.setEnabled(true);
					exportAlphapButton.setEnabled(true);
					exportAlphappButton.setEnabled(true);
					exportHeuristicsButton.setEnabled(true);
				} catch (Exception e1) {
					System.out.println("Derivation of Constraints failed!");
					e1.printStackTrace();
				}
				
				loadConstrainsAndRelations();
				
				switch((String) minersComboBox.getSelectedItem()) {
				case "Declare":
					showDeclareConstraints();
					break;
				case "\u03B1-Algorithm":
					showAlphaRelations();
					break;
				case "\u03B1+-Algorithm":
					showAlphapRelations();
					break;
				case "\u03B1++-Algorithm":
					showAlphappRelations();
					break;
				case "Heuristics-Miner":
					showHeuristicsRelations();
					break;
				default:
					System.out.println("Error with the selection of the miners/algorithms.");
				}
			}
		});
		
		deriveConstraintsButton.setFont(new Font("Arial", Font.PLAIN, 16));
		
		constraintsTabPanel.add(constraintsTabDatabaseNamePanel, BorderLayout.NORTH);
		constraintsTabPanel.add(minerSection, BorderLayout.WEST);
		constraintsTabPanel.add(exportButtons, BorderLayout.EAST);
		constraintsTabPanel.add(dataTableScrollPane, BorderLayout.CENTER);
		constraintsTabPanel.add(deriveConstraintsButton, BorderLayout.SOUTH);
		
		// Import Tab
		main.setLayout(new BorderLayout());
		top.setLayout(new GridLayout(2,1));
		control.setLayout(new GridLayout(6,1,0,20));
		fileHandling.setLayout(new FlowLayout());
		insertHandling.setLayout(new FlowLayout());
		
		fileHandling.add(filenameTextfield);
		filenameTextfield.setEnabled(false);
		fileHandling.add(selectFileButton);
		selectFileButton.addActionListener((e) -> {
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
			fileChooser.showOpenDialog(frame);
			File file = fileChooser.getSelectedFile();
			if (file != null) {
				filenameTextfield.setText(file.getAbsolutePath());
				databaseNameTextField.setText(file.getName().split("\\.")[0]);
			}
		});
		top.add(fileHandling);
		
		insertHandling.add(databaseNameLabel);
		insertHandling.add(databaseNameTextField);
		insertHandling.add(seperatorLabel);
		insertHandling.add(insertLabel);
		insertHandlingButtonGroup.add(newDatabaseRadioButton);
		insertHandlingButtonGroup.add(updateDatabaseRadioButton);
		insertHandling.add(newDatabaseRadioButton);
		newDatabaseRadioButton.setSelected(true);
		insertHandling.add(updateDatabaseRadioButton);
		top.add(insertHandling);		
		
		statusTextArea.setLineWrap(true);
		statusTextArea.setEditable(false);
		statusTextArea.setVisible(true);
		scroll = new JScrollPane(statusTextArea);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		progressBar.setStringPainted(true);
		Border border = BorderFactory.createTitledBorder("Import Process...");
		progressBar.setBorder(border);
		progressBar.setVisible(false);
		
		stopButton.setEnabled(false);
		control.add(startButton);
		startButton.addActionListener((evt) -> {
			try {
				progressBar.setValue(0);
				progressBar.setVisible(true);
				if (newDatabaseRadioButton.isSelected()) {
					presenter.startImport(new File(filenameTextfield.getText()), 
											dbURLTextField.getText(), 
											dbUserNameTextField.getText(), 
											dbPasswordTextField.getText(),
											(String) dbTypeComboBox.getSelectedItem(),
											databaseNameTextField.getText());
				} else {
					presenter.startUpdate(new File(filenameTextfield.getText()), 
							dbURLTextField.getText(), 
							dbUserNameTextField.getText(), 
							dbPasswordTextField.getText(),
							(String) dbTypeComboBox.getSelectedItem(),
							databaseNameTextField.getText());
				}
				
				startButton.setEnabled(false);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		control.add(stopButton);
		stopButton.addActionListener((evt) -> {
			presenter.stopImport();
			startButton.setEnabled(true);
		});
		
		main.add(scroll, BorderLayout.CENTER);
		main.add(progressBar, BorderLayout.SOUTH);
		main.add(top, BorderLayout.NORTH);
		main.add(control, BorderLayout.EAST);
		
		// Database Config Tab
		configureDB.setLayout(new GridBagLayout());
		savedConfigs = new JComboBox(ch.getConfigNames());
		savedConfigs.addActionListener((e) -> {
			showSelectedConfig();
			if (savedConfigs.getSelectedIndex() >= 0){
				Preferences prefsRoot = Preferences.userRoot();
				Preferences myPrefs = prefsRoot.node("org.processmining.plugins.rxesuite");
				myPrefs.putInt("preferredConfig", savedConfigs.getSelectedIndex());
			}
		});
		
		dbTypeComboBox = new JComboBox(ch.supportedDBTypes);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.ipady = 5;
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 1;
		configureDB.add(savedConfigsLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridwidth = 3;
		configureDB.add(savedConfigs, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth = 1;
		configureDB.add(savedConfigNameLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridwidth = 3;
		configureDB.add(savedConfigNameTextField, gbc);
		
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 2;
		
		configureDB.add(new JSeparator(JSeparator.HORIZONTAL), gbc);
		
		gbc.gridwidth = 1;
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		configureDB.add(dbTypeLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.gridwidth = 3;
		configureDB.add(dbTypeComboBox, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridwidth = 1;
		configureDB.add(dbURLLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridwidth = 3;
		configureDB.add(dbURLTextField, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 1;
		configureDB.add(dbUserNameLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.gridwidth = 3;
		configureDB.add(dbUserNameTextField, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridwidth = 1;
		configureDB.add(dbPasswordLabel, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.gridwidth = 3;
		configureDB.add(dbPasswordTextField, gbc);
		
		gbc.anchor = GridBagConstraints.PAGE_END;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(10,0,0,0);
		gbc.gridwidth = 1;
		gbc.weightx = 0.5;
		gbc.weighty = 0.5;
		
		gbc.gridx = 2;
		gbc.gridy = 7;
		configureDB.add(deleteConfigButton, gbc);
		deleteConfigButton.addActionListener((e) -> deleteSelectedConfig());
		
		gbc.gridx = 3;
		gbc.gridy = 7;
		configureDB.add(saveNewConfigButton, gbc);
		saveNewConfigButton.addActionListener((e) -> updateConfig());
		
		showSelectedConfig();
		
		tabbedPane.add(ConstraintsTab, constraintsTabPanel);
		tabbedPane.add(MainTab, main);
		tabbedPane.add(DBConfigTab, configureDB);
		
		tabbedPane.addChangeListener((e) -> {
			if(tabbedPane.getSelectedIndex() == 0 && databaseNameTextField.getText() != ""){
				constraintsTabDatabaseNameTextField.setText(databaseNameTextField.getText());
				frame.requestFocus();
			}
		});
		
		frame.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.pack();
		
		Preferences prefsRoot = Preferences.userRoot();
		Preferences myPrefs = prefsRoot.node("org.processmining.plugins.rxesuite");
		int preferredIndex = myPrefs.getInt("preferredConfig", -1);
		try {
			savedConfigs.setSelectedIndex(preferredIndex);
		} catch (IllegalArgumentException e) {
			savedConfigs.setSelectedIndex(-1);
		}
		
		frame.setVisible(true);
	}

	public void setPresenter(IRXESPresenter presenter) {
		this.presenter = presenter;
	}

	public void log(String s) {
		statusTextArea.append(s+"\n");
		if(statusTextArea.getLineCount() > 100) {
			try {
				int end = statusTextArea.getLineEndOffset(0);
				statusTextArea.replaceRange("", 0, end);
			} catch (BadLocationException e) {
				e.printStackTrace();
			} 
		}
		statusTextArea.setCaretPosition(statusTextArea.getDocument().getLength());
	}
	
	public void processFinished() {
		startButton.setEnabled(true);
	}
	
	public void showSelectedConfig(){
		if (savedConfigs.getSelectedIndex() >= 0) {
			DatabaseConfig config = ch.getConfig((String) savedConfigs.getSelectedItem());
			if(config != null){
				savedConfigNameTextField.setText(config.getConfigName());
				dbTypeComboBox.setSelectedItem(config.getDBtype());
				dbURLTextField.setText(config.getDBurl());
				dbUserNameTextField.setText(config.getDBuser());
				dbPasswordTextField.setText(config.getDBpassword());
			}
		} else {
			savedConfigNameTextField.setText("");
			dbTypeComboBox.setSelectedIndex(-1);
			dbURLTextField.setText("");
			dbUserNameTextField.setText("");
			dbPasswordTextField.setText("");
		}
	}
	
	public void deleteSelectedConfig(){
		try {
			if (savedConfigs.getSelectedIndex() >= 0){
				int deletecheck = 0;
				deletecheck = JOptionPane.showConfirmDialog(frame,
					    		"Do you really want to delete this Configuration?",
					    		"Deletion Warning",
					    		JOptionPane.YES_NO_OPTION);
				
				if(deletecheck == JOptionPane.YES_OPTION) {
					ch.removeConfig(savedConfigs.getSelectedIndex());
					savedConfigs.setModel(new DefaultComboBoxModel(ch.getConfigNames()));
					savedConfigs.setSelectedIndex(-1);
					JOptionPane.showMessageDialog(frame, "Database Configuration deleted!");
					showSelectedConfig();
				}
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(frame, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}

	}

	public void updateConfig(){
			if((savedConfigNameTextField.getText().length() != 0 && 
					dbTypeComboBox.getSelectedIndex() != -1 &&
					dbUserNameTextField.getText().length() != 0 &&
					dbURLTextField.getText().length() != 0) ||
					(dbTypeComboBox.getSelectedItem().equals("SQLite"))) {
				try {
					int overwrite = 0;
					for (String name : ch.getConfigNames()){
						if (savedConfigNameTextField.getText().equals(name)){
							overwrite = JOptionPane.showConfirmDialog(frame,
								    "A Configuration is about to be overwritten. Proceed?",
								    "Overwrite Warning",
								    JOptionPane.YES_NO_OPTION);
						}
					}
					if(overwrite == JOptionPane.YES_OPTION){
						ch.updateConfig(savedConfigNameTextField.getText(), 
								(String)dbTypeComboBox.getSelectedItem(), 
								dbURLTextField.getText(), 
								dbUserNameTextField.getText(), 
								dbPasswordTextField.getText());
						JOptionPane.showMessageDialog(frame, "Database Configurations updated!");
						savedConfigs.setModel(new DefaultComboBoxModel(ch.getConfigNames()));
						savedConfigs.setSelectedItem(savedConfigNameTextField.getText());
					}
				}
				catch (IOException e) {
					JOptionPane.showMessageDialog(frame, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
		} else {
			JOptionPane.showMessageDialog(frame, "Some Required Fields are not filled out!");
		}
	}
	
	public void setProgressBarProgress(int progress){
		progressBar.setValue(progress);
	}

	public void enableStopButton() {
		stopButton.setEnabled(true);
	}
	
	public void loadDeclareConstraints(){
		String[] columnNames = {"Constraint","Support","Confidence"};
		String[][] data;
		try {
			data = presenter.getDeclareConstraints(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			List<String[]> declareConstraints = new ArrayList<String[]>();
			for (int i = 0; i < data.length; i++) {
				if(Double.parseDouble(data[i][3]) >=  Double.parseDouble(supportThresholdTextField.getText()) &&
						Double.parseDouble(data[i][4]) >= Double.parseDouble(confidenceThresholdTextField.getText())) {
					String[] constraint = new String[3];
					if (data[i][0].equals("Init") || data[i][0].equals("End"))
						constraint[0] = data[i][0]+"("+data[i][1]+")";
					else
						constraint[0] = data[i][0]+"("+data[i][1]+","+data[i][2]+")";
					constraint[1] = data[i][3];
					constraint[2] = data[i][4];
					declareConstraints.add(constraint);
				}	
			}
			String[][] declareConstraintsModelData = new String[declareConstraints.size()][3];
			for (int i = 0; i < declareConstraints.size(); i++){
				declareConstraintsModelData[i] = declareConstraints.get(i);
			}
			declareConstraintsModel = new DefaultTableModel(declareConstraintsModelData, columnNames);
		} catch (Exception e) {
			e.printStackTrace();
			declareConstraintsModel = new DefaultTableModel(null, columnNames);
			System.out.println("Problem when displaying DeclareConstraints.");
		}
	}
	
	public void loadAlphaRelations(){
		String[] columnNames = {"Task A", "Relation-Symbol", "Task B"};
		String[][] data;
		
		try {
			
			data = presenter.getDirectSuccessorRelations(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			List<String> directSuccessorRelation = new ArrayList<String>();
			Set<String> activityNames = new HashSet<String>();
			for (int i = 0; i < data.length; i++) {
				directSuccessorRelation.add(data[i][0]+";"+data[i][1]);
				activityNames.add(data[i][0]);
				activityNames.add(data[i][1]);
			}
			List<String> causalityRelation = new ArrayList<String>();
			List<String> concurrencyRelation = new ArrayList<String>();
			List<String> exclusivenessRelation = new ArrayList<String>();
			List<String> startingActivities = new ArrayList<String>();
			List<String> endingActivities = new ArrayList<String>();
			
			for (String name : activityNames) {
				for (String name2 : activityNames) {
					exclusivenessRelation.add(name+";"+name2);
				}
			}
			
			for (String relation : directSuccessorRelation) {
				exclusivenessRelation.remove(relation);
				String[] split = relation.split(";");
				exclusivenessRelation.remove(split[1]+";"+split[0]);
				if (directSuccessorRelation.contains(split[1]+";"+split[0]))
					concurrencyRelation.add(relation);
				else
					causalityRelation.add(relation);
			}
			
			String[] startData = presenter.getStartingActivities(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			String[] endData = presenter.getEndingActivities(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			
			int totalLengthCounter = directSuccessorRelation.size() + exclusivenessRelation.size() + concurrencyRelation.size() + causalityRelation.size() + startData.length + endData.length - 1;
			String[][] alphaModelData = new String[totalLengthCounter+1][3];
			
			for (int i = endData.length - 1; i >= 0; i--) {
				alphaModelData[totalLengthCounter][0] = endData[i];
				alphaModelData[totalLengthCounter][1] = "end";
				alphaModelData[totalLengthCounter][2] = endData[i];
				totalLengthCounter--;
			}
			
			for (int i = startData.length - 1; i >= 0; i--) {
				alphaModelData[totalLengthCounter][0] = startData[i];
				alphaModelData[totalLengthCounter][1] = "start";
				alphaModelData[totalLengthCounter][2] = startData[i];
				totalLengthCounter--;
			}
			
			for(int i = directSuccessorRelation.size()-1; i >= 0; i--) {
				String[] split = directSuccessorRelation.get(i).split(";");
				alphaModelData[totalLengthCounter][0] = split[0];
				alphaModelData[totalLengthCounter][1] = ">";
				alphaModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = exclusivenessRelation.size()-1; i >= 0; i--) {
				String[] split = exclusivenessRelation.get(i).split(";");
				alphaModelData[totalLengthCounter][0] = split[0];
				alphaModelData[totalLengthCounter][1] = "#";
				alphaModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = concurrencyRelation.size()-1; i >= 0; i--) {
				String[] split = concurrencyRelation.get(i).split(";");
				alphaModelData[totalLengthCounter][0] = split[0];
				alphaModelData[totalLengthCounter][1] = "||";
				alphaModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = causalityRelation.size()-1; i >= 0; i--) {
				String[] split = causalityRelation.get(i).split(";");
				alphaModelData[totalLengthCounter][0] = split[0];
				alphaModelData[totalLengthCounter][1] = "->";
				alphaModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
				
			}
			
			alphaModel = new DefaultTableModel(alphaModelData, columnNames);
			
		} catch (Exception e) {
			e.printStackTrace();
			alphaModel = new DefaultTableModel(null,columnNames);
			System.out.println("Problem when displaying Alpha Relations.");
		}
		
	}
	
	public void loadAlphapRelations(){
		String[] columnNames = {"Task A", "Relation-Symbol", "Task B"};
		String[][] data;
		
		try {
			
			data = presenter.getDirectSuccessorRelations(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			List<String> directSuccessorRelation = new ArrayList<String>();
			Set<String> activityNames = new HashSet<String>();
			for (int i = 0; i < data.length; i++) {
				directSuccessorRelation.add(data[i][0]+";"+data[i][1]);
				activityNames.add(data[i][0]);
				activityNames.add(data[i][1]);
			}
			List<String> causalityRelation = new ArrayList<String>();
			List<String> concurrencyRelation = new ArrayList<String>();
			List<String> exclusivenessRelation = new ArrayList<String>();
			List<String> length2LoopRelation = new ArrayList<String>();
			List<String> twoWaylength2LoopRelation = new ArrayList<String>();
			
			for (String name : activityNames) {
				for (String name2 : activityNames) {
					exclusivenessRelation.add(name+";"+name2);
				}
			}
			
			String[][] length2Loops = presenter.getLength2LoopRelations(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			
			for(int i = 0; i < length2Loops.length; i++) {
				length2LoopRelation.add(length2Loops[i][0]+";"+length2Loops[i][1]);
			}
			
			for (String loop : length2LoopRelation) {
				String[] split = loop.split(";");
				if(length2LoopRelation.contains(split[1]+";"+split[0])) {
					twoWaylength2LoopRelation.add(loop);
				}
			}
			
			for (String relation : directSuccessorRelation) {
				exclusivenessRelation.remove(relation);
				String[] split = relation.split(";");
				exclusivenessRelation.remove(split[1]+";"+split[0]);
				if (directSuccessorRelation.contains(split[1]+";"+split[0]) &&
						!twoWaylength2LoopRelation.contains(relation))
					concurrencyRelation.add(relation);
				if (!directSuccessorRelation.contains(split[1]+";"+split[0]) ||
						twoWaylength2LoopRelation.contains(relation))
					causalityRelation.add(relation);
			}
			
			String[] startData = presenter.getStartingActivities(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			String[] endData = presenter.getEndingActivities(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			
			int totalLengthCounter = directSuccessorRelation.size() + exclusivenessRelation.size() 
									+ concurrencyRelation.size() + causalityRelation.size() 
									+ length2LoopRelation.size() + twoWaylength2LoopRelation.size() 
									+ startData.length + endData.length - 1;
			
			String[][] alphapModelData = new String[totalLengthCounter+1][3];
		
			for (int i = endData.length - 1; i >= 0; i--) {
				alphapModelData[totalLengthCounter][0] = endData[i];
				alphapModelData[totalLengthCounter][1] = "end";
				alphapModelData[totalLengthCounter][2] = endData[i];
				totalLengthCounter--;
			}
			
			for (int i = startData.length - 1; i >= 0; i--) {
				alphapModelData[totalLengthCounter][0] = startData[i];
				alphapModelData[totalLengthCounter][1] = "start";
				alphapModelData[totalLengthCounter][2] = startData[i];
				totalLengthCounter--;
			}
			
			for(int i = directSuccessorRelation.size()-1; i >= 0; i--) {
				String[] split = directSuccessorRelation.get(i).split(";");
				alphapModelData[totalLengthCounter][0] = split[0];
				alphapModelData[totalLengthCounter][1] = ">";
				alphapModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = exclusivenessRelation.size()-1; i >= 0; i--) {
				String[] split = exclusivenessRelation.get(i).split(";");
				alphapModelData[totalLengthCounter][0] = split[0];
				alphapModelData[totalLengthCounter][1] = "#";
				alphapModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = twoWaylength2LoopRelation.size()-1; i >= 0; i--) {
				String[] split = twoWaylength2LoopRelation.get(i).split(";");
				alphapModelData[totalLengthCounter][0] = split[0];
				alphapModelData[totalLengthCounter][1] = "\u2662";
				alphapModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = length2LoopRelation.size()-1; i >= 0; i--) {
				String[] split = length2LoopRelation.get(i).split(";");
				alphapModelData[totalLengthCounter][0] = split[0];
				alphapModelData[totalLengthCounter][1] = "\u25B3";
				alphapModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = concurrencyRelation.size()-1; i >= 0; i--) {
				String[] split = concurrencyRelation.get(i).split(";");
				alphapModelData[totalLengthCounter][0] = split[0];
				alphapModelData[totalLengthCounter][1] = "||";
				alphapModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = causalityRelation.size()-1; i >= 0; i--) {
				String[] split = causalityRelation.get(i).split(";");
				alphapModelData[totalLengthCounter][0] = split[0];
				alphapModelData[totalLengthCounter][1] = "->";
				alphapModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
				
			}
			
			alphapModel = new DefaultTableModel(alphapModelData, columnNames);
			
		} catch (Exception e) {
			e.printStackTrace();
			alphapModel = new DefaultTableModel(null,columnNames);
			System.out.println("Problem when displaying Heuristics-Miner Relations.");
		}
	}
	
	public void loadAlphappRelations(){
		String[] columnNames = {"Task A", "Relation-Symbol", "Task B"};
		String[][] data;
		
		try {
			
			data = presenter.getDirectSuccessorRelations(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			List<String> directSuccessorRelation = new ArrayList<String>();
			Set<String> activityNames = new HashSet<String>();
			for (int i = 0; i < data.length; i++) {
				directSuccessorRelation.add(data[i][0]+";"+data[i][1]);
				activityNames.add(data[i][0]);
				activityNames.add(data[i][1]);
			}
			
			List<String> causalityRelation = new ArrayList<String>();
			List<String> concurrencyRelation = new ArrayList<String>();
			List<String> exclusivenessRelation = new ArrayList<String>();
			List<String> length2LoopRelation = new ArrayList<String>();
			List<String> xorSplitRelation = new ArrayList<String>();
			List<String> xorJoinRelation = new ArrayList<String>();
			List<String> indirectSuccessionRelation = new ArrayList<String>();
			List<String> successionRelation = new ArrayList<String>();
			List<String> longDistanceDependencyRelation = new ArrayList<String>();
			
			for (String name : activityNames) {
				for (String name2 : activityNames) {
					exclusivenessRelation.add(name+";"+name2);
				}
			}
			
			String[][] length2Loops = presenter.getLength2LoopRelations(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			
			for(int i = 0; i < length2Loops.length; i++) {
				length2LoopRelation.add(length2Loops[i][0]+";"+length2Loops[i][1]);
			}
			
			for (String relation : directSuccessorRelation) {
				String[] split = relation.split(";");
				exclusivenessRelation.remove(relation);
				exclusivenessRelation.remove(split[1]+";"+split[0]);
				if (directSuccessorRelation.contains(split[1]+";"+split[0]) &&
						!(length2LoopRelation.contains(relation) || length2LoopRelation.contains(split[1]+";"+split[0])))
					concurrencyRelation.add(relation);
				if (!directSuccessorRelation.contains(split[1]+";"+split[0]) ||
						length2LoopRelation.contains(relation) || length2LoopRelation.contains(split[1]+";"+split[0]))
					causalityRelation.add(relation);
			}
			
			//XOR Split Relation
			for (String relation :  exclusivenessRelation){
				String[] split = relation.split(";");
				String a = split[0];
				String b = split[1];
				if (a.equals(b)) continue;
				for (String c : activityNames) {
					if(causalityRelation.contains(c+";"+a) && causalityRelation.contains(c+";"+b)){
						xorSplitRelation.add(a+";"+b);
					}
				}
			}
			
			//XOR Join Relation
			for (String relation : exclusivenessRelation){
				String[] split = relation.split(";");
				String a = split[0];
				String b = split[1];
				if (a.equals(b)) continue;
				for (String c: activityNames) {
					if(causalityRelation.contains(a+";"+c) && causalityRelation.contains(b+";"+c)){
						xorJoinRelation.add(a+";"+b);
					}
				}
			}
			
			String[][] longDistanceDependencies = presenter.getLongDistanceDependencyRelations(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			
			for(int i = 0; i < longDistanceDependencies.length; i++){
				longDistanceDependencyRelation.add(longDistanceDependencies[i][0]+";"+longDistanceDependencies[i][1]);
			}
			
			//Indirect Succession
			//The Problem when deriving the Indirect Succession Relation is that the actual traces have to be analyze to determine
			//if there are XOR-Splits/Joins between two activities.
			//This fact means that the derivation of all necessary relations for the alpha plus plus miner is not possible by just 
			//querying the declare contraints in the database. Hence, the alpha-plus-plus algorithm is not really suited for this approach
			//However, the functionality to derive the alpha-plus-plus constraints is still added to this tool to show, that it is nevertheless
			//possible to derive the constraints (by other means; and with worse performance compared to the others)
			ArrayList<ArrayList<String>> traces = presenter.getTraces(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			relations:
			for (String relation : exclusivenessRelation){
				String[] split = relation.split(";");
				String a = split[0];
				String b = split[1];
				for(ArrayList<String> trace : traces){
					int posA = -1;
					int posB = -1;
					for(int i = 0; i < trace.size(); i++){
						if (trace.get(i).equals(a)){
							posA=i;
							for(int j = posA+1; j < trace.size(); j++){
								if(trace.get(j).equals(a)){
									break;
								}
								if(trace.get(j).equals(b)){
									posB = j;
									boolean indirectSuccession = true;
									for(int k = posA+1; k <= posB-1; k++){
										String tk = trace.get(k);
										if(xorJoinRelation.contains(tk+";"+a) && xorSplitRelation.contains(tk+";"+a)) {
											indirectSuccession = false;
										}
									}
									if(indirectSuccession == true){
										indirectSuccessionRelation.add(a+";"+b);
										continue relations;
									}
								}
							}
						}
					}
				}
			}
			
			//Succession
			successionRelation.addAll(causalityRelation);
			successionRelation.addAll(indirectSuccessionRelation);
			
			String[] startData = presenter.getStartingActivities(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			String[] endData = presenter.getEndingActivities(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			
			int totalLengthCounter = directSuccessorRelation.size() + exclusivenessRelation.size() 
									+ concurrencyRelation.size() + causalityRelation.size() 
									+ length2LoopRelation.size() + xorJoinRelation.size()
									+ xorSplitRelation.size() + indirectSuccessionRelation.size()
									+ successionRelation.size() + startData.length + endData.length - 1;
			
			String[][] alphappModelData = new String[totalLengthCounter+1][3];
		
			for (int i = endData.length - 1; i >= 0; i--) {
				alphappModelData[totalLengthCounter][0] = endData[i];
				alphappModelData[totalLengthCounter][1] = "end";
				alphappModelData[totalLengthCounter][2] = endData[i];
				totalLengthCounter--;
			}
			
			for (int i = startData.length - 1; i >= 0; i--) {
				alphappModelData[totalLengthCounter][0] = startData[i];
				alphappModelData[totalLengthCounter][1] = "start";
				alphappModelData[totalLengthCounter][2] = startData[i];
				totalLengthCounter--;
			}
			
			for(int i = successionRelation.size()-1; i >= 0; i--) {
				String[] split = successionRelation.get(i).split(";");
				alphappModelData[totalLengthCounter][0] = split[0];
				alphappModelData[totalLengthCounter][1] = "\u227B";
				alphappModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = directSuccessorRelation.size()-1; i >= 0; i--) {
				String[] split = directSuccessorRelation.get(i).split(";");
				alphappModelData[totalLengthCounter][0] = split[0];
				alphappModelData[totalLengthCounter][1] = ">";
				alphappModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = indirectSuccessionRelation.size()-1; i >= 0; i--) {
				String[] split = indirectSuccessionRelation.get(i).split(";");
				alphappModelData[totalLengthCounter][0] = split[0];
				alphappModelData[totalLengthCounter][1] = ">>";
				alphappModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = exclusivenessRelation.size()-1; i >= 0; i--) {
				String[] split = exclusivenessRelation.get(i).split(";");
				alphappModelData[totalLengthCounter][0] = split[0];
				alphappModelData[totalLengthCounter][1] = "#";
				alphappModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for (int i = xorJoinRelation.size()-1; i >= 0; i--) {
				String[] split = xorJoinRelation.get(i).split(";");
				alphappModelData[totalLengthCounter][0] = split[0];
				alphappModelData[totalLengthCounter][1] = "\u25B7";
				alphappModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for (int i = xorSplitRelation.size()-1; i >= 0; i--) {
				String[] split = xorSplitRelation.get(i).split(";");
				alphappModelData[totalLengthCounter][0] = split[0];
				alphappModelData[totalLengthCounter][1] = "\u25C1";
				alphappModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = length2LoopRelation.size()-1; i >= 0; i--) {
				String[] split = length2LoopRelation.get(i).split(";");
				alphappModelData[totalLengthCounter][0] = split[0];
				alphappModelData[totalLengthCounter][1] = "\u25B3";
				alphappModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = concurrencyRelation.size()-1; i >= 0; i--) {
				String[] split = concurrencyRelation.get(i).split(";");
				alphappModelData[totalLengthCounter][0] = split[0];
				alphappModelData[totalLengthCounter][1] = "||";
				alphappModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = causalityRelation.size()-1; i >= 0; i--) {
				String[] split = causalityRelation.get(i).split(";");
				alphappModelData[totalLengthCounter][0] = split[0];
				alphappModelData[totalLengthCounter][1] = "->";
				alphappModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			alphappModel = new DefaultTableModel(alphappModelData, columnNames);
			
		} catch (Exception e) {
			e.printStackTrace();
			alphapModel = new DefaultTableModel(null,columnNames);
			System.out.println("Problem when displaying Heuristics-Miner Relations.");
		}
	}
	
	public void loadHeuristicsRelations() {
		String[] columnNames = {"Task A", "Relation-Symbol", "Task B"};
		String[][] data;
		
		try {
			
			data = presenter.getDirectSuccessorRelations(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			List<String> directSuccessorRelation = new ArrayList<String>();
			Set<String> activityNames = new HashSet<String>();
			for (int i = 0; i < data.length; i++) {
				directSuccessorRelation.add(data[i][0]+";"+data[i][1]);
				activityNames.add(data[i][0]);
				activityNames.add(data[i][1]);
			}
			List<String> causalityRelation = new ArrayList<String>();
			List<String> concurrencyRelation = new ArrayList<String>();
			List<String> exclusivenessRelation = new ArrayList<String>();
			List<String> length2LoopRelation = new ArrayList<String>();
			List<String> longDistanceDependencyRelation = new ArrayList<String>();
			
			for (String name : activityNames) {
				for (String name2 : activityNames) {
					exclusivenessRelation.add(name+";"+name2);
				}
			}
			
			for (String relation : directSuccessorRelation) {
				exclusivenessRelation.remove(relation);
				String[] split = relation.split(";");
				exclusivenessRelation.remove(split[1]+";"+split[0]);
				if (directSuccessorRelation.contains(split[1]+";"+split[0]))
					concurrencyRelation.add(relation);
				else
					causalityRelation.add(relation);
			}
			
			String[][] length2Loops = presenter.getLength2LoopRelations(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			
			for(int i = 0; i < length2Loops.length; i++) {
				length2LoopRelation.add(length2Loops[i][0]+";"+length2Loops[i][1]);
			}
			
			String[][] longDistanceDependencies = presenter.getLongDistanceDependencyRelations(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			
			for(int i = 0; i < longDistanceDependencies.length; i++){
				longDistanceDependencyRelation.add(longDistanceDependencies[i][0]+";"+longDistanceDependencies[i][1]);
			}
			
			String[] startData = presenter.getStartingActivities(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			String[] endData = presenter.getEndingActivities(dbURLTextField.getText(), dbUserNameTextField.getText(), dbPasswordTextField.getText(), (String) dbTypeComboBox.getSelectedItem(), constraintsTabDatabaseNameTextField.getText());
			
			int totalLengthCounter = directSuccessorRelation.size() + exclusivenessRelation.size() 
									+ concurrencyRelation.size() + causalityRelation.size() 
									+ length2LoopRelation.size() + longDistanceDependencyRelation.size()
									+ startData.length + endData.length - 1;
			
			String[][] heuristicsModelData = new String[totalLengthCounter+1][3];
		
			for (int i = endData.length - 1; i >= 0; i--) {
				heuristicsModelData[totalLengthCounter][0] = endData[i];
				heuristicsModelData[totalLengthCounter][1] = "end";
				heuristicsModelData[totalLengthCounter][2] = endData[i];
				totalLengthCounter--;
			}
			
			for (int i = startData.length - 1; i >= 0; i--) {
				heuristicsModelData[totalLengthCounter][0] = startData[i];
				heuristicsModelData[totalLengthCounter][1] = "start";
				heuristicsModelData[totalLengthCounter][2] = startData[i];
				totalLengthCounter--;
			}
			
			for(int i = directSuccessorRelation.size()-1; i >= 0; i--) {
				String[] split = directSuccessorRelation.get(i).split(";");
				heuristicsModelData[totalLengthCounter][0] = split[0];
				heuristicsModelData[totalLengthCounter][1] = ">";
				heuristicsModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = exclusivenessRelation.size()-1; i >= 0; i--) {
				String[] split = exclusivenessRelation.get(i).split(";");
				heuristicsModelData[totalLengthCounter][0] = split[0];
				heuristicsModelData[totalLengthCounter][1] = "#";
				heuristicsModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = longDistanceDependencyRelation.size()-1; i >= 0; i--) {
				String[] split = longDistanceDependencyRelation.get(i).split(";");
				heuristicsModelData[totalLengthCounter][0] = split[0];
				heuristicsModelData[totalLengthCounter][1] = ">>>";
				heuristicsModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = length2LoopRelation.size()-1; i >= 0; i--) {
				String[] split = length2LoopRelation.get(i).split(";");
				heuristicsModelData[totalLengthCounter][0] = split[0];
				heuristicsModelData[totalLengthCounter][1] = ">>";
				heuristicsModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = concurrencyRelation.size()-1; i >= 0; i--) {
				String[] split = concurrencyRelation.get(i).split(";");
				heuristicsModelData[totalLengthCounter][0] = split[0];
				heuristicsModelData[totalLengthCounter][1] = "||";
				heuristicsModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
			}
			
			for(int i = causalityRelation.size()-1; i >= 0; i--) {
				String[] split = causalityRelation.get(i).split(";");
				heuristicsModelData[totalLengthCounter][0] = split[0];
				heuristicsModelData[totalLengthCounter][1] = "->";
				heuristicsModelData[totalLengthCounter][2] = split[1];
				totalLengthCounter--;
				
			}
			
			heuristicsModel = new DefaultTableModel(heuristicsModelData, columnNames);
			
		} catch (Exception e) {
			e.printStackTrace();
			heuristicsModel = new DefaultTableModel(null,columnNames);
			System.out.println("Problem when displaying Heuristics-Miner Relations.");
		}
	}
	
	private void loadConstrainsAndRelations() {
		loadDeclareConstraints();
		loadAlphaRelations();
		loadAlphapRelations();
		loadAlphappRelations();
		loadHeuristicsRelations();
	}
	
	private void showDeclareConstraints() {
		dataTable.setModel(declareConstraintsModel);
	}
	
	private void showAlphaRelations() {
		dataTable.setModel(alphaModel);
	}
	
	private void showAlphapRelations(){
		dataTable.setModel(alphapModel);
	}
	
	private void showAlphappRelations(){
		dataTable.setModel(alphappModel);
	}
	
	private void showHeuristicsRelations(){
		dataTable.setModel(heuristicsModel);
	}
	
	public void exportTableModelAsCSV(DefaultTableModel model, String filename) {
		JFileChooser CSVFileChooser = new JFileChooser();
		CSVFileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
		CSVFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		CSVFileChooser.showSaveDialog(frame);
		boolean writeOk = true;
		File directory = CSVFileChooser.getSelectedFile();
		
		if (directory != null){
			
			File output = new File(directory.getAbsolutePath(),filename);
			
			if (output.exists()){
				writeOk = false;
				int result = JOptionPane.showConfirmDialog(frame,filename+" already exists in this directory, overwrite?","Existing file",JOptionPane.YES_NO_CANCEL_OPTION);
				if (result == JOptionPane.YES_OPTION){
					writeOk = true;
				}
			}
			
			try {
				if(writeOk){
					//BufferedWriter writer = new BufferedWriter(new FileWriter(output));
					BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), "UTF8"));
					for (int i = 0; i < model.getColumnCount(); i++) {
						if (i != model.getColumnCount()-1)
							writer.write(model.getColumnName(i)+";");
						else
							writer.write(model.getColumnName(i));
					}
					
					writer.write("\n");
					
					for (int i = 0; i < model.getRowCount(); i++) {
						for (int j = 0; j < model.getColumnCount(); j++) {
							if(j != model.getColumnCount()-1)
								writer.write(model.getValueAt(i, j).toString()+";");
							else
								writer.write(model.getValueAt(i, j).toString());
						}
						writer.write("\n");
					}
					
					JOptionPane.showMessageDialog(frame, filename + " created successfully.", "Success!", JOptionPane.INFORMATION_MESSAGE);
					writer.close();
					
				}		
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error while writing to file.");
			} 
		
		} else {
			JOptionPane.showMessageDialog(frame, "No Directory selected.", "Error!", JOptionPane.WARNING_MESSAGE);
		}
	}
}
