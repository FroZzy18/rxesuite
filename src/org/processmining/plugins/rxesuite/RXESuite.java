package org.processmining.plugins.rxesuite;

import java.io.IOException;

public class RXESuite {
	public static void main(String[] args) throws IOException{
		MainView view = new MainView();
		view.setPresenter(new MainPresenter(view));
	}
}
