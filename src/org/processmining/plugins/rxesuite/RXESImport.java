package org.processmining.plugins.rxesuite;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.factory.XFactoryNaiveImpl;
import org.deckfour.xes.id.XIDFactory;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.util.XTimer;


public class RXESImport implements Runnable{
	
	Map<String, String> attributeCache = new HashMap<String, String>();
	DatabaseHandler dbhandler;
	File source;
	IRXESPresenter logger;
	
	public RXESImport(DatabaseHandler dbhandler, File f, IRXESPresenter logger){
		this.dbhandler = dbhandler;
		this.source = f;
		this.logger = logger;
	}
	
	
	public String generateXID(){
		return XIDFactory.instance().createId().toString().replaceAll("-", "").toUpperCase();
	}
	
	public String getAttributeType(String type){
		
		switch (type) {
		case "XAttributeLiteralImpl":	type = "string";
										break;
										
		case "XAttributeBooleanImpl": 	type = "boolean";
										break;
										
		case "XAttributeContainerImpl":	type = "container";
										break;
										
		case "XAttributeDiscreteImpl":	type = "float";
										break;
		
		case "XAttributeContinousImpl":	type = "float";
										break;
										
		case "XAttributeIDImpl":		type = "id";
										break;
										
		case "XAttributeListImpl":		type = "list";
										break;
										
		case "XAttributeTimestampImpl":	type ="date";
										break;
		
		default:						break;
		
		}
		
		return type;
	}
	
	public String getAttributeId(XAttribute attribute, String parentId) throws Exception{
		
		String type = getAttributeType(attribute.getClass().getSimpleName());
		String identifier = attribute.getKey() +" "+ type;
		
		if (attributeCache.get(identifier) == null) {
	
			String attributeId = generateXID();
			attributeCache.put(identifier, attributeId);
			XExtension extension = attribute.getExtension();
			String extensionId = null;
			if (extension != null)
				extensionId = dbhandler.getExtensionID(extension.getName(), extension.getPrefix(), extension.getUri());
			logger.log("New Attribute discovered, inserting into attribute-table:");
			logger.log("AttributeID: "+attributeId+", Key: "+attribute.getKey()+", Type: "+type+", ParentID: "+parentId+", ExtensionId: "+extensionId);
			dbhandler.insertAttribute(attributeId, attribute.getKey(), type, parentId, extensionId);
			
		}
		
		if (attribute.hasAttributes()) {
			XAttributeMap attributemap = attribute.getAttributes();
			
			for (XAttribute childattribute : attributemap.values()) {
				getAttributeId(childattribute, attributeCache.get(identifier));
			}

		}
		
		return attributeCache.get(identifier);
		
	}
	
	public String getAttributeId(XAttribute attribute) throws Exception{
		return getAttributeId(attribute, null);
	}
	
	public void run(){
		try {
			XTimer timer = new XTimer();
			timer.start();
			logger.log("Import process started.");
			XesXmlParser parser = new XesXmlParser(new XFactoryNaiveImpl());
			logger.log("Setting up database structure....");
			dbhandler.rxesSetup();
			logger.log("Disabling foreign key checks...");
			dbhandler.disableChecks();
	        FileInputStream in = new FileInputStream(source);
	        logger.log("Parsing .xes file...");
	        XLog log = parser.parse(in).get(0);
	        logger.log("File succesfully parsed!");
	        
	        XLogInfo loginfo = XLogInfoFactory.createLogInfo(log);
	        double numberOfEvents = loginfo.getNumberOfEvents();
	        double processedEvents = 0.0;
	        int numberOfTraces = loginfo.getNumberOfTraces();
	        logger.log("Traces: "+numberOfTraces+", Events: "+numberOfEvents);
	        
	        logger.log("-----------------------");
	        
	        String logId = generateXID();
	
	        XAttributeMap logmap= log.getAttributes();
	        logger.log("Inserting Log: "+source.getName()+" ID: "+logId);
	        dbhandler.insertLog(logId, source.getName());
	        
	        logger.log("-------------------------");
	        
	        Set<XExtension> extensionSet = log.getExtensions();
	        for (XExtension extension : extensionSet) {
	        	String extensionId = generateXID();
	        	logger.log("Inserting Extension:");
	        	logger.log("ID: "+extensionId+", Name: "+extension.getName()+ ", Prefix: "+extension.getPrefix()+", URI: "+extension.getUri());
	        	dbhandler.insertExtension(extensionId, extension.getName(), extension.getPrefix(), extension.getUri());
	        }
	        
	        logger.log("----------------------------");
	        
	        List<XAttribute> globalTraceAttributeList = log.getGlobalTraceAttributes();
	        for (XAttribute globalTraceAttribute : globalTraceAttributeList) {
	        	logger.log("Inserting GlobalTrace Attribute:");
	        	logger.log("Key: "+globalTraceAttribute.getKey()+", Value: "+globalTraceAttribute.toString());
				dbhandler.insertLog_has_Attribute(logId, true, false, getAttributeId(globalTraceAttribute), globalTraceAttribute.toString());
	        }
	        
	        logger.log("--------------------------");
	        
	        List<XAttribute> globalEventAttributeList = log.getGlobalEventAttributes();
	        for (XAttribute globalEventAttribute : globalEventAttributeList) {
	        	logger.log("Inserting GlobalEvent Attribute:");
	        	logger.log("Key: "+globalEventAttribute.getKey()+", Value: "+globalEventAttribute.toString());
				dbhandler.insertLog_has_Attribute(logId, false, true, getAttributeId(globalEventAttribute), globalEventAttribute.toString());
	        }
	        
	        logger.log("---------------------------");
	        
	        for(XAttribute logAttribute : logmap.values()) {
	        	logger.log("Inserting Log Attribute:");
	        	logger.log("Key: "+logAttribute.getKey()+", Value: "+logAttribute.toString());
				dbhandler.insertLog_has_Attribute(logId, false, false, getAttributeId(logAttribute), logAttribute.toString());
			}
	        
	        logger.log("---------------------------");
	        
	        List<XEventClassifier> classifiersList = log.getClassifiers();
	        for (XEventClassifier classifier : classifiersList) {
	        	String[] attr_keys_array = classifier.getDefiningAttributeKeys();
	        	StringBuilder attr_keys_builder = new StringBuilder();
	        	
	        	for (String key : attr_keys_array) {
	        		attr_keys_builder.append(key+" ");
	        	}
	        	
	        	String attr_keys = attr_keys_builder.toString();
	        	attr_keys = attr_keys.substring(0, attr_keys.length() - 1);
	        	
	        	String classifierId = generateXID();
	        	logger.log("Inserting Classifier:");
	        	logger.log("ID: "+classifierId+", Name: "+classifier.name()+ ", Keys: "+attr_keys);
	        	dbhandler.insertClassifier(classifierId, classifier.name(), attr_keys, logId);
	        }
	        
	        logger.log("----------------------------");
	        
	        long tracesequence = 0;
			for (XTrace trace : log) {
				tracesequence++;
				String traceId = generateXID();
				logger.log("Inserting Trace, ID: "+traceId);
				dbhandler.insertTrace(traceId);
				dbhandler.insertLog_has_Trace(logId, traceId, tracesequence);
				if(trace.hasAttributes()){
					XAttributeMap tracemap = trace.getAttributes();
					for (XAttribute traceAttribute : tracemap.values()) {
						logger.log("Inserting Trace Attribute:");
			        	logger.log("Key: "+traceAttribute.getKey()+", Value: "+traceAttribute.toString());
						dbhandler.insertTrace_has_Attribute(traceId, getAttributeId(traceAttribute), traceAttribute.toString());	
					}
				}
				
				logger.log("................................");
				
				long eventsequence = 0;
				for (XEvent event : trace) {
					eventsequence++;
					processedEvents++;
					String eventId = event.getID().toString().replaceAll("-", "").toUpperCase();
					logger.log("Inserting Event, ID:"+eventId);
					dbhandler.insertEvent(eventId);
					dbhandler.insertTrace_has_Event(traceId, eventId, eventsequence);
					if(event.hasAttributes()){
						XAttributeMap eventmap = event.getAttributes();
						for (XAttribute eventAttribute : eventmap.values()) {
							logger.log("Inserting Event Attribute:");
				        	logger.log("Key: "+eventAttribute.getKey()+", Value: "+eventAttribute.toString());
							dbhandler.insertEvent_has_Attribute(eventId, getAttributeId(eventAttribute), eventAttribute.toString());	
						}
						logger.log("...............................");
					}
				}
				
				logger.log("----------------------------", (int)((processedEvents/numberOfEvents)*100));
				
			} 
			
			timer.stop();
			dbhandler.enableChecks();
			logger.log("Enabling foreign key checks...");
			logger.log("The whole Process took: " +timer.getDurationString() +", for "+numberOfTraces+" Traces and "+numberOfEvents+" Events");
			logger.processFinished();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
