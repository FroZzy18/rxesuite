package org.processmining.plugins.rxesuite;

public class DatabaseConfig {
	
	private String ConfigName;
	private String DBurl;
	private String DBuser;
	private String DBpassword;
	private String DBtype;

	public DatabaseConfig(String configName, String DBtype, String DBurl, String DBuser, String DBpassword) {
		this.setConfigName(configName);
		this.setDBtype(DBtype);
		this.setDBurl(DBurl);
		this.setDBuser(DBuser);
		this.setDBpassword(DBpassword);
	}

	public String getDBurl() {
		return DBurl;
	}

	public void setDBurl(String dBurl) {
		DBurl = dBurl;
	}

	public String getDBuser() {
		return DBuser;
	}

	public void setDBuser(String dBuser) {
		DBuser = dBuser;
	}

	public String getDBpassword() {
		return DBpassword;
	}

	public void setDBpassword(String dBpassword) {
		DBpassword = dBpassword;
	}

	public String getDBtype() {
		return DBtype;
	}

	public void setDBtype(String dBtype) {
		DBtype = dBtype;
	}

	public String getConfigName() {
		return ConfigName;
	}

	public void setConfigName(String configName) {
		ConfigName = configName;
	}

}
