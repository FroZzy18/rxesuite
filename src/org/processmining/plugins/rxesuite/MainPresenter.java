package org.processmining.plugins.rxesuite;

import java.io.File;
import java.util.ArrayList;

public class MainPresenter implements IRXESPresenter{
	
	private IMainView view;
	private Thread thread;
	
	public MainPresenter(MainView view){
		this.view = view;
	}

	public void log(String s) {
		view.log(s);
	}
	
	public void startImport(File file, String url, String username, String password, String driver, String dbName) throws Exception{
		DatabaseHandler dbhandler = null;
		if (driver == "SQLite"){
			dbhandler = new DatabaseHandlerStringID(url, username, password, driver, dbName);
		} else if (driver == "MicrosoftSQL"){
			dbhandler = new DatabaseHandlerMicrosoftSQL(url,username,password,driver,dbName);
			view.enableStopButton();
		}
		else {
			dbhandler = new DatabaseHandler(url, username, password, driver, dbName);
			view.enableStopButton();
		}
		RXESImport process = new RXESImport(dbhandler, file, this);
		thread = new Thread(process);
		thread.start();
	}
	
	public void startUpdate(File file, String url, String username, String password, String driver, String dbName) throws Exception{
		DatabaseHandler dbhandler = null;
		if (driver == "SQLite"){
			dbhandler = new DatabaseHandlerStringID(url, username, password, driver, dbName);
		} else if (driver == "MicrosoftSQL") {
			dbhandler = new DatabaseHandlerMicrosoftSQL(url,username,password,driver,dbName);
			view.enableStopButton();
		} 
		else {
			dbhandler = new DatabaseHandler(url, username, password, driver, dbName);
			view.enableStopButton();
		}
		RXESUpdate process = new RXESUpdate(dbhandler, file, this);
		thread = new Thread(process);
		thread.start();
	}

	public void stopImport() {
		thread.stop();
	}
	
	public void processFinished() {
		view.processFinished();
	}

	public void log(String s, int progress) {
		if (s != null)
			view.log(s);
		view.setProgressBarProgress(progress);
	}
	
	public DatabaseHandler createConnection(String url, String username, String password, String driver, String dbName) throws Exception{
		DatabaseHandler dbhandler = null;
		if (driver == "SQLite"){
			dbhandler = new DatabaseHandlerStringID(url, username, password, driver, dbName);
		} else if (driver == "MicrosoftSQL") {
			dbhandler = new DatabaseHandlerMicrosoftSQL(url,username,password,driver,dbName);
			view.enableStopButton();
		} 
		else {
			dbhandler = new DatabaseHandler(url, username, password, driver, dbName);
			view.enableStopButton();
		}
		
		return dbhandler;
	}

	public String[][] getDeclareConstraints(String url, String username, String password, String driver, String dbName) throws Exception {
		DatabaseHandler dbhandler = createConnection(url, username, password, driver, dbName);
		return dbhandler.getDeclareConstraints();
	}
	
	public String[][] getDirectSuccessorRelations(String url, String username, String password, String driver, String dbName) throws Exception {
		DatabaseHandler dbhandler = createConnection(url, username, password, driver, dbName);
		return dbhandler.getDirectSuccessorRelations();
	}
	
	public String[][] getLength2LoopRelations(String url, String username, String password, String driver, String dbName) throws Exception {
		DatabaseHandler dbhandler = createConnection(url, username, password, driver, dbName);
		return dbhandler.getLength2LoopRelations();
	}
	
	public String[][] getLongDistanceDependencyRelations(String url, String username, String password, String driver, String dbName) throws Exception{
		DatabaseHandler dbhandler = createConnection(url, username, password, driver, dbName);
		return dbhandler.getLongDistanceDependencyRelations();
	}
	
	public String[] getStartingActivities(String url, String username, String password, String driver, String dbName) throws Exception{
		DatabaseHandler dbhandler = createConnection(url, username, password, driver, dbName);
		return dbhandler.getStartingActivities();
	}
	
	public String[] getEndingActivities(String url, String username, String password, String driver, String dbName) throws Exception{
		DatabaseHandler dbhandler = createConnection(url, username, password, driver, dbName);
		return dbhandler.getEndingActivities();
	}
	
	public ArrayList<ArrayList<String>> getTraces(String url, String username, String password, String driver, String dbName) throws Exception{
		DatabaseHandler dbhandler = createConnection(url, username, password, driver, dbName);
		return dbhandler.getTraces();
	}

	public void deriveDeclarativeConstraints(String[] declarativeConstraints, String activityFieldName, String url, String username, String password, String driver, String dbName) throws Exception {
		DatabaseHandler dbhandler = createConnection(url, username, password, driver, dbName);
		dbhandler.populateLogFlat(activityFieldName);
		for (String constraint:declarativeConstraints){
			switch (constraint) {
			case "Response":
				dbhandler.deriveResponseConstraints();
				break;
			case "AlternateResponse":
				dbhandler.deriveAlternateResponseConstraints();
				break;
			case "ChainResponse":
				dbhandler.deriveChainResponseConstraints();
				break;
			case "Precedence":
				dbhandler.derivePrecedenceConstraints();
				break;
			case "AlternatePrecedence":
				dbhandler.deriveAlternatePrecedenceConstraints();
				break;
			case "ChainPrecedence":
				dbhandler.deriveChainPrecedenceConstraints();
				break;
			case "RespondedExistance":
				dbhandler.deriveRespondedExistanceConstraints();
				break;
			case "NotSuccession":
				dbhandler.deriveNotSuccessionConstraints();
				break;
			case "Length_2_Loop":
				dbhandler.deriveLength2LoopConstraints();
				break;
			case "Init":
				dbhandler.deriveInitConstraints();
				break;
			case "End":
				dbhandler.deriveEndConstraints();
				break;
			default:
				System.out.println("Specified constraint not available!");
			}
		}
	}

}
