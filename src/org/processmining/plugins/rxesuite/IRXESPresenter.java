package org.processmining.plugins.rxesuite;

import java.io.File;
import java.util.ArrayList;

public interface IRXESPresenter {
	public void log(String s);
	public void log(String s, int progress);
	public void startImport(File file, String url, String username, String password, String driver, String dbName) throws Exception;
	public void processFinished();
	public void stopImport();
	public void startUpdate(File file, String url, String username, String password, String driver, String dbName) throws Exception;
	public String[][] getDeclareConstraints(String url, String username, String password, String driver, String dbName) throws Exception;
	public String[][] getDirectSuccessorRelations(String url, String username, String password, String driver, String dbName) throws Exception;
	public String[][] getLength2LoopRelations(String url, String username, String password, String driver, String dbName) throws Exception;
	public String[][] getLongDistanceDependencyRelations(String url, String username, String password, String driver, String dbName) throws Exception;
	public String[] getStartingActivities(String url, String username, String password, String driver, String dbName) throws Exception;
	public String[] getEndingActivities(String url, String username, String password, String driver, String dbName) throws Exception;
	public ArrayList<ArrayList<String>> getTraces(String url, String username, String password, String driver, String dbName) throws Exception;
	public void deriveDeclarativeConstraints(String[] declarativeConstraints, String activityFieldName, String url, String username, String password, String driver, String dbName) throws Exception;
}
