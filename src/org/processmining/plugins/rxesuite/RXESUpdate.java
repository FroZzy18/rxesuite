package org.processmining.plugins.rxesuite;

import java.io.File;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.factory.XFactoryNaiveImpl;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XLogInfoFactory;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.util.XTimer;

public class RXESUpdate extends RXESImport {
	
	private boolean isNewLog = false;

	public RXESUpdate(DatabaseHandler dbhandler, File f, IRXESPresenter logger) {
		super(dbhandler, f, logger);
	}
	
	public String getLogId(String LogName) throws SQLException{
		String logId = dbhandler.getLogId(LogName);
		
		if(logId != null) {
			return logId;
		}
		
		isNewLog = true;
		return generateXID();
	}
	
	public void run(){
		try {
			XTimer timer = new XTimer();
			timer.start();
			logger.log("Update process started.");
			XesXmlParser parser = new XesXmlParser(new XFactoryNaiveImpl());
			logger.log("Analyzing database structure....");
			logger.log("Disabling constraint checks to speed up import process...");
			dbhandler.disableChecks();
			attributeCache = dbhandler.getAttributeMap(); // Builds the in-memory attribute structure
	        FileInputStream in = new FileInputStream(source);
	        logger.log("Parsing .xes file...");
	        XLog log = parser.parse(in).get(0);
	        logger.log("File succesfully parsed!");
	        
	        XLogInfo loginfo = XLogInfoFactory.createLogInfo(log);
	        double numberOfEvents = loginfo.getNumberOfEvents();
	        double processedEvents = 0;
	        int numberOfTraces = loginfo.getNumberOfTraces();
	        logger.log("Traces: "+numberOfTraces+", Events: "+numberOfEvents);
	        
	        logger.log("-----------------------");
	        
	        String logId = getLogId(source.getName());  
	
	        XAttributeMap logmap= log.getAttributes();
	        
	        if (isNewLog){
		        logger.log("Inserting Log: "+source.getName()+" ID: "+logId);
		        dbhandler.insertLog(logId, source.getName());
	        } else {
	        	logger.log("Updating Log: "+source.getName()+" ID: "+logId);
	        }
	        
	        logger.log("-------------------------");
	        
	        Set<XExtension> extensionSet = log.getExtensions();
	        for (XExtension extension : extensionSet) {
	        	if (dbhandler.getExtensionID(extension.getName(), extension.getPrefix(), extension.getUri()) == null ){
		        	String extensionId = generateXID();
		        	logger.log("Inserting Extension:");
		        	logger.log("ID: "+extensionId+", Name: "+extension.getName()+ ", Prefix: "+extension.getPrefix()+", URI: "+extension.getUri());
		        	dbhandler.insertExtension(extensionId, extension.getName(), extension.getPrefix(), extension.getUri());
	        	} else {
	        		logger.log("Skipping Extension: Name: "+extension.getName()+ ", Prefix: "+extension.getPrefix()+", URI: "+extension.getUri());
	        		logger.log("Reason: Already in Database.");
	        	}
	        	logger.log("............................");
	        }
	        
	        logger.log("----------------------------");
	        
	        List<XAttribute> globalTraceAttributeList = log.getGlobalTraceAttributes();
	        for (XAttribute globalTraceAttribute : globalTraceAttributeList) {
	        	try {
		        	logger.log("Trying to insert GlobalTrace Attribute:");
		        	logger.log("Key: "+globalTraceAttribute.getKey()+", Value: "+globalTraceAttribute.toString());
					dbhandler.insertLog_has_Attribute(logId, true, false, getAttributeId(globalTraceAttribute), globalTraceAttribute.toString());
	        	} catch (SQLException e) {
	        		logger.log("GlobalTrace Attribute not inserted.");
	        		logger.log("Reason: GlobalTrace Attribute already in Database.");
	        	}
	        	logger.log("............................");
	        }
	        
	        logger.log("--------------------------");
	        
	        List<XAttribute> globalEventAttributeList = log.getGlobalEventAttributes();
	        for (XAttribute globalEventAttribute : globalEventAttributeList) {
	        	try {
	        		logger.log("Trying to inserting GlobalEvent Attribute:");
	        		logger.log("Key: "+globalEventAttribute.getKey()+", Value: "+globalEventAttribute.toString());
	        		dbhandler.insertLog_has_Attribute(logId, false, true, getAttributeId(globalEventAttribute), globalEventAttribute.toString());
	        	} catch (SQLException e) {
	        		logger.log("GlobalEvent Attribute not inserted.");
	        		logger.log("Reason: GlobalEvent Attribute already in Database.");
	        	}
	        	logger.log("............................");
	        }
	        
	        logger.log("---------------------------");
	        
	        for(XAttribute logAttribute : logmap.values()) {
	        	try {
		        	logger.log("Trying to inserting Log Attribute:");
		        	logger.log("Key: "+logAttribute.getKey()+", Value: "+logAttribute.toString());
					dbhandler.insertLog_has_Attribute(logId, false, false, getAttributeId(logAttribute), logAttribute.toString());
	        	} catch (SQLException e) {
	        		logger.log("Log Attribute not inserted.");
	        		logger.log("Reason: Log Attribute already in Database.");
	        	}
	        	logger.log("............................");
			}
	        
	        logger.log("---------------------------");
	        
	        List<XEventClassifier> classifiersList = log.getClassifiers();
	        for (XEventClassifier classifier : classifiersList) {
	        	String[] attr_keys_array = classifier.getDefiningAttributeKeys();
	        	StringBuilder attr_keys_builder = new StringBuilder();
	        	
	        	for (String key : attr_keys_array) {
	        		attr_keys_builder.append(key+" ");
	        	}
	        	
	        	String attr_keys = attr_keys_builder.toString();
	        	attr_keys = attr_keys.substring(0, attr_keys.length() - 1);
	        	
	        	if (dbhandler.getClassifierId(classifier.name(), attr_keys, logId) == null) {
		        	String classifierId = generateXID();
		        	logger.log("Inserting Classifier:");
		        	logger.log("ID: "+classifierId+", Name: "+classifier.name()+ ", Keys: "+attr_keys);
		        	dbhandler.insertClassifier(classifierId, classifier.name(), attr_keys, logId);
	        	} else {
	        		logger.log("Skipping Classifier: Name: "+classifier.name()+ ", Keys: "+attr_keys);
	        		logger.log("Reason: Classifier already in Database.");
	        	}
	        	logger.log("............................");
	        }
	        
	        logger.log("----------------------------");
	        
	        long tracesequence = 0;
			for (XTrace trace : log) {
				tracesequence++;
				String traceId = generateXID();
				logger.log("Inserting Trace, ID: "+traceId);
				dbhandler.insertTrace(traceId);
				dbhandler.insertLog_has_Trace(logId, traceId, tracesequence);
				if(trace.hasAttributes()){
					XAttributeMap tracemap = trace.getAttributes();
					for (XAttribute traceAttribute : tracemap.values()) {
						logger.log("Inserting Trace Attribute:");
			        	logger.log("Key: "+traceAttribute.getKey()+", Value: "+traceAttribute.toString());
						dbhandler.insertTrace_has_Attribute(traceId, getAttributeId(traceAttribute), traceAttribute.toString());	
					}
				}
				
				logger.log("................................");
				
				long eventsequence = 0;
				for (XEvent event : trace) {
					eventsequence++;
					processedEvents++;
					String eventId = event.getID().toString().replaceAll("-", "").toUpperCase();
					logger.log("Inserting Event, ID:"+eventId);
					dbhandler.insertEvent(eventId);
					dbhandler.insertTrace_has_Event(traceId, eventId, eventsequence);
					if(event.hasAttributes()){
						XAttributeMap eventmap = event.getAttributes();
						for (XAttribute eventAttribute : eventmap.values()) {
							logger.log("Inserting Event Attribute:");
				        	logger.log("Key: "+eventAttribute.getKey()+", Value: "+eventAttribute.toString());
							dbhandler.insertEvent_has_Attribute(eventId, getAttributeId(eventAttribute), eventAttribute.toString());	
						}
						logger.log("...............................");
					}
				}
				
				logger.log("----------------------------", (int)((processedEvents/numberOfEvents)*100));
				
			} 
			
			timer.stop();
			logger.log("Enabling constraint checks...");
			dbhandler.enableChecks();
			logger.log("The whole Process took: " +timer.getDurationString() +", for "+numberOfTraces+" Traces and "+numberOfEvents+" Events");
			logger.processFinished();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
