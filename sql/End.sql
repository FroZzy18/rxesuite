select 'End', Task, Task,
(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(distinct Instance) FROM log_flat) AS FLOAT)) AS Support,
(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(distinct Instance) FROM log_flat) AS FLOAT)) * (CAST((SELECT COUNT(distinct Instance) FROM log_flat i WHERE i.Task = o.Task ) AS FLOAT)) / CAST((SELECT COUNT(distinct Instance) FROM log_flat) AS FLOAT) AS Confidence	
FROM log_flat o
WHERE o.Time = (SELECT MAX(i2.Time) from log_flat i2 WHERE o.Instance = i2.Instance)
GROUP BY Task;