SELECT 'alternateResponse', TaskA, TaskB,
(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) AS Support,	
((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskA GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence
FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x
WHERE a.Task = x.TaskA AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskB AND b.Instance = a.Instance AND b.Time > a.Time)
 		AND NOT EXISTS(SELECT *  FROM log_flat b, log_flat c WHERE c.Instance = a.Instance AND c.Task = x.TaskA AND b.Instance = a.Instance AND b.Task = x.TaskB AND c.Time > a.Time AND c.Time < b.Time)
GROUP BY x.TaskA, x.TaskB
HAVING (CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) > 0.7 AND
 ((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM(SELECT Instance FROM log_flat WHERE Task LIKE TaskA GROUP BY Instance)t2) AS FLOAT) / CAST((SELECT COUNT(*) FROM(SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) > 0.5