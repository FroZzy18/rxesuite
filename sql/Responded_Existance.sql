SELECT 'respondedExistence', TaskA, TaskB,
(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) AS Support,	
((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE TaskA GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence
FROM log_flat a, (SELECT a.Task AS TaskA, b.Task AS TaskB FROM log_flat a, log_flat b WHERE a.Task != b.Task GROUP BY a.Task, b.Task) x
WHERE a.Task = x.TaskB AND EXISTS (SELECT * FROM log_flat b WHERE b.Task = x.TaskA AND b.Instance = a.Instance)
GROUP BY x.TaskA, x.TaskB
HAVING (CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) > 0.7 AND
 ((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE TaskA) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM(SELECT Instance FROM log_flat WHERE Task LIKE TaskA GROUP BY Instance)t2) AS FLOAT) / CAST((SELECT COUNT(*) FROM(SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) > 0.5