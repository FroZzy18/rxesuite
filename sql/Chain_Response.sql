SELECT 'ChainResponse', a.Task as 'TaskA', b.Task as 'TaskB',
(CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE a.Task) AS FLOAT)) AS Support,	
((CAST(COUNT(*) AS FLOAT)/CAST((SELECT COUNT(*) FROM log_flat WHERE Task LIKE a.Task) AS FLOAT)) * (CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat WHERE Task LIKE a.Task GROUP BY Instance)t2) AS FLOAT)/CAST((SELECT COUNT(*) FROM (SELECT Instance FROM log_flat GROUP BY Instance) t) AS FLOAT))) AS Confidence
FROM log_flat a, log_flat b
WHERE a.Instance = b.Instance
AND b.Time = a.Time+1
GROUP BY a.Task, b.Task