CREATE TABLE log_flat(
EventID BINARY(16),
Instance BINARY(16),
Task VARCHAR(200),
Time BIGINT);

insert into log_flat select the.event_id, trace_id, eha.value, the.sequence from trace_has_event the INNER JOIN event e on the.event_id=e.id 
INNER JOIN event_has_attribute eha ON e.id = eha.event_id
INNER JOIN attribute a ON a.id = eha.attribute_id
WHERE a.attr_key like 'concept:name'
ORDER BY trace_id, sequence	
GO